.. _chapter-end-user-label:


=====================
End User Requirements
=====================

------------
Installation
------------

Hunter uses FlightGear's `multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ feature, such that several pilots can see the same targets. All of Hunter's processing is on a remote computer (server). Unless you want to run Hunter yourself, nothing has to be installed locally on your machine. I.e. there is no `Nasal <http://wiki.flightgear.org/Nasal_scripting_language>`_ or `AI <http://wiki.flightgear.org/AI_Traffic>`_ module to download and install.

The availability of Hunter is currently announced by user ``HB-VANO`` on the `OPRF Discord <https://discordapp.com/channels/228041550687371264/228041550687371264>`_ server in the ``hunter-asset-manager`` channel. Use the same channel to request Hunter running with a specific scenario. If you want to run your own Hunter processor, just reach out to ``HB-VANO``.


---------------------
Download of MP assets
---------------------

In order to be able to see the targets properly, shoot at them and get hits, you need to use a compatible FlightGear aircraft from `Operation Red Flag (OPRF) <http://opredflag.com/>`_ and enable MP messaging.
Use a recent FlightGear version (at least 2019.1+), which is compatible with the OPRF aircraft.

You need to have a recent version (optimally latest version) of `OPRF Assets <https://github.com/l0k1/oprf_assets>`_ and `US Navy Ships <https://github.com/NikolaiVChr/u.s.navy>`_.

In order to see helicopters and drones, you need the following aircraft:

* FGUK's `Eurocopter EC665 Tiger <http://www.fguk.me/hangar/rotary/download/13-rotary-wing/518-eurocopter-ec665-tiger>`_
* FGUK's `Boing CH-47 Chinook <http://www.fguk.me/hangar/rotary/download/13-rotary-wing/490-ch-47-hc-versions>`_
* `FGAddOn hangar <http://wiki.flightgear.org/FGAddon>`_ Mil-Mi-24
* `Mil Mi-8 <https://github.com/RenanMsV/Mil-Mi-8>`_
* `MQ-9 <https://github.com/JMaverick16/MQ-9>`_

In order to see automated attacking planes, you need the following aircraft:

* `F-15 <https://github.com/Zaretto/fg-aircraft/tree/OPRF>`_
* `F-16 <https://github.com/NikolaiVChr/f16>`_
* `SU-27 <https://github.com/yanes19/SU-27SK>`_
* `MiG-29 <https://github.com/alexeijd/MiG-29_9-12>`_

Only for the Swiss Shooting Ranges scenario, you need additionally the following aircraft:

* `Pilatus PC-9M <https://sourceforge.net/p/flightgear/fgaddon/HEAD/tree/trunk/Aircraft/PC-9M/>`_

..  `Northrop F-20 Tigershark <https://github.com/JMaverick16/F-20>`_ (simulating the F-5E flown by the Swiss Air Force)


If you are participating when there is a MultiPlayer scenario including a carrier, then you have the get the MPCarrier from the `FGAddOn hangar <http://wiki.flightgear.org/FGAddon>`_ (this is different from the carrier AI scenarios, where the carrier models are available by default).

If you are participating when there is a MultiPlayer scenario including an ait-to-air tanker/refuler, then you also need to get the `KC-137R family <https://github.com/JMaverick16/KC-137R>`_.


The `Emesary multiplayer bridge for FlightGear <http://chateau-logic.com/content/emesary-multiplayer-bridge-flightgear>`_ is the default in OPRF and therefore you need to use a compatible OPRF aircraft.


===============
End User Flying
===============

You need to connect to the OPRF's MP-server `<mpserver.opredflag.com>`_. When you are connected, all objects are represented as MP-targets, the same way you would see another player's aircraft.

In order that your shooting at targets actually leads to impact, you need to be registered - which happens automatically. "Impact" means that with sufficient destructive power an asset visibly takes damage and after some time goes away. Ships stop moving after some impact - and sink, when they are destroyed. Helicopters fall out of the sky.

If Hunter is in realistic mode, then the callsigns are 'OPFORxx' and thereby hiding the type of the target. Otherwise the callsigns of the targets are built up like follows (e.g. ``xvehi57``):

* 1 letter identifier (per default `x`)
* Up to 4 letters type
* Two digits running index number

The following types exist:

* building (e.g. depot)
* vehicle (e.g. truck, buk-m2)
* ship (e.g. frigate)
* helicopter
* plane
* unknown

A target's callsign can be reused later for a new and different target. I.e. just because a target was once destroyed you cannot count on that the callsign will be gone.

Depending on the scenario, some of the ships, SAMs or fighter aircraft shoot at you - watch out and know your key binding for chaff/flares!


----------------------------------------
Ground Controlled Interception Interface
----------------------------------------

Some OPRF planes like the `MiG-21bis <https://github.com/l0k1/MiG-21bis>`_, the `Mirage 2000-* <https://github.com/5H1N0B11/Aircraft>`_ and the `F-16 <https://github.com/NikolaiVChr/f16>`_ have menu items or key bindings to request information about flying enemies from a GCI-station:

* PICTURE - full tactical picture
* BOGEY DOPE - BRAA of nearest target
* CUTOFF - vector to nearest target

While the `Ground control interception (GCI) <https://en.wikipedia.org/wiki/Ground-controlled_interception>`_ in OPRF assets implements this as intended, a less realistic feature using the same mechanism can be used in Hunter to find ground-attack targets:

* PICTURE - returns the BRAA of the nearest ship
* BOGEY DOPE - returns the BRAA of the nearest land vehicle (in most scenarios all kinds of vehicles like SAMs, trucks etc. are returned - however, each scenario can specify that only specific types of vehicles are returned)
* CUTOFF - returns the vector to the nearest helicopter or drone (if you are not yet airborne, then additional 30 seconds are added to the ETA assuming an average speed of ca. 400 kt - you can always make a request later again, when you are airborne and at speed to get better indications)

New requests will only be processed 10 seconds after the last one.

If the same request type is sent within 2 minutes again, updated information for the same target will be displayed (if the target is still alive) - no matter whether the target is still the nearest one or not anymore.

It can be that no target seems to be available - that includes situations for CUTOFF, where the target due to aspect and/or speed difference cannot be intercepted. That might change if you increase the speed of your own aircraft and the calculation therefore turns to your advantage.

No information can be requested for static targets like buildings - but their coordinates might be described in a specific scenario. Also, no POPUP calls will come in.

Bearing information is showing magnetic north based on the `magnetic declination <https://en.wikipedia.org/wiki/Magnetic_declination>`_, i.e. not geographic/true north. Most fighters will use magnetic North in the HUD and in the instruments, but e.g. the Mirage 2000 has a button where you can switch between magnetic and true north.


----------------
Radar of targets
----------------
The following target types have active radar:

* warships
* GCI
* SAMs
* fighter aircraft

If a target's health is broken or it is destroyed, then the radar is turned off also on these target types. Some targets might turn the radar off and on based on heuristics taking the distance to attacking aircraft into account. Some SAMs might randomly turn their radar on and off.


-------------------
Damaging of targets
-------------------

Hunter uses the same mechanism for damage as OPRF Assets. It means that you need to enable multiplayer protocol version 2 and have MP messaging on.

Each target has a number of hitpoints: the stronger built a target is (e.g. a frigate vs. a truck) the higher the number of hitpoints. Each weapon has a strength in hitpoints: bullets have far fewer hitpoints than a missile. When a target is hit, the weapon's hitpoints reduces the hitpoints of the target based on some heuristics (e.g. distance from the target, a bit of randomness). The heuristics in Hunter are the same as in OPRF assets (if it exists), otherwise they are similar. While the hitpoints for weapons are the same as in OPRF assets (``damage.nas``), the hitpoints are the same in most cases.

A target's health has five states:

* ``fit for fight``: not been hit yet
* ``hit``: some reduction of hitpoints. Depending on the attacking aircraft's modelling, some functions might not work anymore
* ``broken``: when the target's hitpoints have been reduced to below a certain threshold (currently 33%), then the target is just handled as a brick. It does not move/fly anymore and radar is off.
* ``destroyed``: hitpoints have reached 0. Explodes and starts burning (if the model supports it). A ship begins to sink.
* ``dead``: after an amount of time depending on the target, the target is removed from MP and not visible anymore. A new target of any type at any time could pop up again using the same callsign.


-------
Carrier
-------
For landing and taking off from a carrier, you need to follow the instructions closely - amongst others you have to set the additional command line property correctly - or you will not see the carrier. Use the following resources:

* The general description of `how to use carriers <http://wiki.flightgear.org/Howto:Carrier>`_ in FlightGear . Includes amongst others the TACAN codes to use in finding the carrier.
* You need to follow at least the installation pilot sections of the `Carrier over MP <http://wiki.flightgear.org/Carrier_over_MP>`_ wiki article.
* The F-14 is currently the only carrier capable aircraft in OPRF with weapons. There is an extensive description of `how to land on a carrier <http://wiki.flightgear.org/Howto:Carrier_Landing>`_.

The person launching the scenario will announce which carrier (e.g. the Vinson) is active and which callsign it has (needed to configure the property in FG). Most often it will be the Vinson with callsign MP_029X

NB: as of FG version 2020.1 you cannot spawn on a carrier. However, once landed on a carrier you can launch on the carrier.

Save yourself frustrations by:

* Closely following the instructions for MP carriers. You will not gain time by just skimming the wiki - you need to read and understand.
* Train carrier recoveries with very little fuel and using AI scenarios. It is difficult and the F-14 is a complex and difficult beast. Crashing after a successful hunt and then also having to re-spawn on land instead of launching on the carrier is not fun.

=========
Scenarios
=========

---------------
Northern Norway
---------------

A scenario around `ENNA <https://en.wikipedia.org/wiki/Lakselv_Airport,_Banak>`_ in the very North of Norway with fjords and ships and all. Lots of bombing targets and enough terrain changes to make hunting a bit interesting if you fly fast and low.

An `osm2city <http://wiki.flightgear.org/Osm2city.py>`_ scenery is available for `download <https://my.pcloud.com/publink/show?code=XZNq6r7Za3MElP74oG7xL5VN1wTW5pA2uX4k>`_.

In the map below ships are sailing along the green lines, helicopters are flying along the red lines and static targets are place within the lila areas.

.. image:: enna_map_large.png

The following table lists a set of static targets, which can be hit by GPS-coordinates. Other static targets (e.g. SAMs) can be found due to active radar. Around these static targets there might be additional targets, e.g. trucks.

=======    ===========    =================    ============
Airport    Type           lon, lat             Altitude (m)
=======    ===========    =================    ============
ENAT       Shelter        23.3548, 69.9757     9
ENAT       Hangar         23.3793, 69.9728     3
ENHF       Shelter        23.6673, 70.6805     81
ENHF       Shelter        23.6752, 70.6820     80
ENHF       Gasometer      23.3513, 70.6789     76
ENHF       Gasometer      23.6793, 70.6803     77
ENHK       Shelter        22.1347, 70.4861     5
ENHK       Shelter        22.1421, 70.4848     14
ENHV       Hangar         25.9764, 71.0091     3
ENNA       Shelter        24.9694, 70.0598     12
ENNA       Shelter        24.9780, 70.0686     5
ENNA       Shelter        24.9643, 70.0757     3
=======    ===========    =================    ============

There might also be an aircraft carrier available sailing in the North-West corner of the map. Additionally, there could be a tanker available in the same area, flying in a pattern from north of Arnøya North-North-East ca. 40 nm and then returning back on FL120.

---------------------
Swiss Shooting Ranges
---------------------

.....
Axalp
.....

The `Swiss Air Force <https://en.wikipedia.org/wiki/Swiss_Air_Force>`_ has a live shooting range at ca. 2200 metres above mean sea level in the Alps near `Meiringen airbase (ICAO: LSMM) <https://www.vtg.admin.ch/en/organisation/kdo-op/air-force/flpl-mei.html>`_ on `Axalp <https://en.wikipedia.org/wiki/Axalp#Fliegerschiessen_Axalp>`_ (Ebenfluh). There are plenty of videos and pictures on the internet to give an impression about the spectacular scenery. The `core area <https://www.openstreetmap.org/node/305235603#map=14/46.6985/8.0744>`_ is between the Axalphorn and the Schwarzhorn.

There are 3 red shooting targets (ca. 10 m * 5 m) for cannons, 2 of which can also be used with rockets (not the one at "Axalphorn" in north direction).

The Swiss Air Force has an exercise "EK03", which previously was called "Kleeblatt" (cloverleaf, shamrock) as the flight path is looking like it. Due to tight turns and significant height differences most parts are flown with afterburner. The speed should never be below 320 kt and at the point of shooting should be up to 450-500 kt. Only during the 2-3 seconds of cannon fire there is a stable flight path. A detailed description can be read in [1]_, [2]_ and [3]_ (see manipulated pictures from the book below).


.. image:: axalp_a_c.png


.. image:: axalp_d_f.png


You start from LSMM, fly west to the lake "Brienzersee" and gain height to ca. 7500 ft while flying along the lake towards the town "Interlaken" until you have enough height, speed and your cannon is ready to fire. Finally turn left and fly North-East towards the valley between the "Axalphorn" and the "Schwarzhorn" mountains. You fly through the vally, do not shoot on the target, pass the command tower (on your left side) while flying over the "Grätli" (ridge) and then turn sharp to left between the "Axalphorn" and the "Oltschiburg" peaks in order to dive into the "Urserli" valley in north direction.

Flying the following 6 phases takes ca. 3 minutes depending on the fighter aircraft and skills.:

A. **(blue)** Fly a somewhat continuous 270 degree left turn along the mountain side gaining height while double checking that your cannon is ready. You get into the same valley as before, but this time you shoot a short burst against one of the two targets just under the ridge (preferably use the left target). Fly again over "Grätli" and dive into the "Urserli" valley.
B. **(green)** Fly a larger left turn this time, such that the flight path ends east-south-east and leads a bit to the left of the command post directly against the target in the cliff. After a short burst pull up to be able to gain height quickly and roll over the ridge (ca. 9000 ft).
C. **(red)** Dive a bit and turn right 180 degrees while gaining height again to fly in north direction towards the peak of "Schwarzhorn". To the left of the peak there is a gap, through which you fly north and then dive towards the second target near "Axalphorn". Pull the trigger, raise the nose a bit and show off by rolling left onto the back and dive over the ridge, roll 90 degrees and try to fly a bit east towards the airport.
D. **(amber)** Make a sharp 210 degree turn left towards the mountains and then over the lake and gain height immediately, so that you are at least at 7500 ft and can see the third target on a cliff (in FlightGear just a steep mountain side) while flying over the ridge between "Axalphorn" peak and the command tower. Shoot a burst and then immediately start a steep climb to roll over the ridge to the right of "Schwarzhorn" (same gap as used in the pass before in the other direction).
E. **(pink)** Make a large right turn while gaining height to cross the ridge near "Grossenegg" peak (ca. 3 km to the left of "Schwarzhorn") in north-east direction. You might shortly see the command post, but keep turning right and dive to hide a bit - just to pull up again and along a mountain side turn right into the valley with the first target. As soon as you see the first target (preferably the one on the right side) you need to aim and shoot. Continue in a soft long right turn towards north-east to fly around the "Garzen" peak.
F. **(brown)** Keep turning to the right in a large bow around the "Schwarzhorn" and fly north-north-east to the left of the "Grossenegg" to finally dive towards the target near "Axalphorn", shoot and roll over the mountain. From there you can start over at A.


.. image:: axalp_helicopter.png


Remember to switch the targets to get hits registered - that is quite demanding compared to real life (unless you fly e.g. the `MiG-21bis <https://github.com/l0k1/MiG-21bis>`_ or the `Mirage 2000 <https://github.com/5H1N0B11/Aircraft>`_, where it is not needed). Carry just enough fuel - but else get rid of all other weight like missiles and stuff. It takes practice to get it right and you will have to remember the geometry of the flight path and of the mountains. Imagine a spotter near the targets: the less seconds he would see you, the better landscape cover you have, which is what you need in a war scenario.

.. image:: axalp_all_targets.png


..........
Dammastock
..........

Ca. 8 nm South-East of LSMM lies the ``Dammastock`` peak in the middle of a mountain and glacier landscape, which is surrounded by 4 pass roads.

.. image:: schiessplatz_dammastock.png

There are 2 Pilatus PC-9 towing an orange target in a distance of 500 metres from the plane on FL120 (in real life FL140) while flying in a circle with a diameter of 10 km around the peak. Simulated attacks can be flows along the whole circle, however shooting is only allowed in the zone from ca. 6 o'clock to 9o'clock (see picture).

Most often 2 modes of attack are flown:

* Flying on an elevated larger circle than the towed target and then dive down from out-side in (called "Avanti").
* "Butterfly": first fly against the towing plane and then fly a curve with pull-up/pull-down to hit the target from the front/side.

Apart from hitting the target and narrow landscape, timing is the biggest challenge.

You can also use the towing planes to train air policing slow targets.

.....
Forel
.....

Ca. in the middle of the East shore of the Lake of Neuchâtel there is a target in the water close to the village of ``Forel``.

.. image:: forel_sea_target.png

As in real life the target is difficult to see and you need to watch out for your altitude over ground when shooting from a dive.

The following picture illustrates the location relative to landmarks. On the left side within blue colour the military airport of Payerne (``LSMP``) can be seen. Pink shows a break in teh forest along the shore and two marks in the water. Yellow is the location of the water target.

.. image:: forel_sitation.png


.............................
Small And Fast Ground Targets
.............................

If you fly from ``LSMM`` less than 10 nm South-South-West, then you get to a former military airport ``LSMI``. A truck is driving with up to 100 km/h on the taxiways and the runway in a random pattern.

.. image:: truck_lsmi.png

Flying instead North-North-East for ca. 15 nm brings you to a part of Lake Lucerne just at the end of the Runway of ``LSMA``, where a fast speedboat waits to be shot at as a target.

NB: these are not real life situations of the Swiss Air Force. The targets will only get hits but no damage. Challenge yourself by flying without radar or flying low and seek terrain coverage. You might need to fly in fair weather to be able to see the targets.

.. image:: speedboat_lsma_2.png


There is a convoy of 3 trucks driving around within ca. 5 km of ``LSMP`` (which is located a few minutes fighter flying west of LSMM). An other convoy of 5 trucks drives on streets along the border of lake ``Murtensee`` a few kilometers north of ``LSMP``.


.. [1] Peter Lewis, 2017: Swiss Tiger - Parallel Flight. Descriptions in German and English on pages 17-19.

.. [2] Olivier Borgeaud, Peter Gunti, 2011: Mirage - Das fliegende Dreieck. Description on page 313 in German.

.. [3] Olivier Borgeauf, Peter Gunti, Peter Lewis, 2010: Hunter - Ein Jäger für die Schweiz. 3D pictures on page 254.


..................
Helicopter Hunting
..................
In the surroundings of the Lake of Lucerne (Vierwaldstättersee) a bunch of low flying helicopters wait to be hunted. The area starts just north of ``LSMM`` up until a bit north of ``LSME``. ``LSZC`` is a previous military airport and a good starting point. The whole area is from 8-8.75 degrees East and 46.75-47.125 North.


------------------------------
Eugen Kvaternik (aka. Croatia)
------------------------------


.. image:: eugen_kvaternik_entrance.jpeg


This is a Croatian military shooting range near Slunj. There is a Croatian `wikipedia entry <https://hr.wikipedia.org/wiki/Vojni_poligon_%22Eugen_Kvaternik%22>`_ describing it.

The scenario in Hunter is not a correct representation of the range. Instead the targets have been placed such that they are easy to spot. In the northern side it is easiest to orient oneself with the help of the rivers.

In addition to static targets there are 2 speedboats around the 4 islands ``Prvić``, ``Sveti Grgur``, ``Goli otok`` and ``Rab`` to the South-East of the island of ``Krk``, on which the `Rijeka airport (LDRI) <https://en.wikipedia.org/wiki/Rijeka_Airport>`_ is situated. Alternative airports are the `Aviano Air Base (LIPA) <https://en.wikipedia.org/wiki/Aviano_Air_Base>`_ or `Zadar (LDZD) <https://en.wikipedia.org/wiki/Zadar_Airport>`_.

There is a convoy of `Humvees <https://en.wikipedia.org/wiki/Humvee>`_ in a clearing to the West of the town of Slunj. Like the 2 speedboats the trucks are immortal.


.. image:: croatia_convoy.png


On the opposite side of the shooting range seen from Slunj in direction South-West there is a little ``Blaćansko`` lake/wetland. Cf. the following picture: in the lake there are three targets and to the North-East up the hill are two static targets.


.. image:: croatia_lake_hill_targets.png


If a carrier has been announced, then it is situated between the Italian and Croatian coast in the `Adriatic Sea <https://en.wikipedia.org/wiki/Adriatic_Sea>`_.

If a tanker has been announced, then it is departing from ``LIPA``, follows the Italian coast and then flies a pattern ca. between `Rimini <https://en.wikipedia.org/wiki/Rimini>`_ and `Zadar <https://en.wikipedia.org/wiki/Zadar>`_.


----------
Grafenwöhr
----------

The `Grafenwöhr shooting range <https://de.wikipedia.org/wiki/Truppen%C3%BCbungsplatz_Grafenw%C3%B6hr>`_ includes two small airstrips ``ETIC`` and ``ETOI`` with a runway length of ca. 1 km. If you need a longer runway, then ``ETSI`` (`Fliegerhost Ingolstadt/Manching <https://de.wikipedia.org/wiki/Fliegerhorst_Ingolstadt/Manching>`_) or ``ETSN`` (`Fliegerhorst Neuburg <https://de.wikipedia.org/wiki/Fliegerhorst_Neuburg>`_) might be more suitable.

There are lots of static targets as well as a few moving trucks. The positions and types of target are not a direct mapping from reality. This scenario is for ground attack in an not very demanding topography only.


------
Vidsel
------

A scenario at the `Vidsel test range <https://en.wikipedia.org/wiki/Vidsel_Test_Range>`_ next to ``ESPE``. Other notable nearby airports are Jokkmokk (``ESNJ``) and Luleå (``ESPA``).

Targets are located approximately 30nm bearing 330 from ``ESPE``. There are three groups of targets, at ``19.31E,66.40N`` (a few buildings, heavy SAM and AAA), ``19.21E,66.31N`` (target containers, hard shelters, some SAM and AAA), and ``19.47E,66.26N`` (industrial buildings), plus several small gun targets.

There is `custom terrain <https://gitlab.com/colingeniet/ESPE-scenery>`_ available for this scenario.


=====================
Creation of Scenarios
=====================

The documentation of the scenario format is in the quite extensive comments in the code: `scenarios.py <https://gitlab.com/vanosten/hunter/-/blob/master/hunter/scenarios.py>`_. There is no graphical user interface to create scenarios on a map like in DCS, BSM Falcon, IL-2 series etc. Currently all scenarios are in the same file as the code.

Some hints:

* Have a look at existing scenarios and try to understand - although it is "code", the code being written in Python is readable without programming knowledge.
* The most difficult and tedious part is defining networks, as it has to be done by hand. Use e.g. the `FlightGear map over scenery models <https://scenery.flightgear.org/map/>`_ to get coordinates (NB: Hunter's format is lon/lat not lat/lon - and you need to use decimal notation for coordinates). You can use a big screen and semi-transparent paper, draw the outlines, decide on the network nodes and numbering and then on the edges (and in the end keep a photo of it). Using this the encoding (list all nodes, then add all nodes to the network, then connect nodes as edges) gets quite easy.
* Even if you do not understand everything, it helps a lot if you do as much as possible in the given format and then coordinate with e.g. the Hunter maintainer to collect the pieces.
* To make convoys or towing (e.g. a plane with a towed target som 500 metres behind) you need to use steps in activation_delay (e.g. 2 seconds between trucks in a convoy or 500 m / 100 m/s  = 5 secs between tractor and towed object), use the same network and use one of the following: either a DiGraph circle for constant speed moving targets or the same list of destinations for routed trip targets.
* For routed trips:
    * You have to look at a `OpenStreetMap <https://www.openstreetmap.org/#map=7/56.188/11.617>`_ and make sure that the network of roads actually allows to go from the origin to the sequence of destinations. If a path cannot be found, then the target just does not move and a warning is written to the log. But you know that only while executing - there is no ahead of time validation currently.
    * The smaller the network, the better.

When generating networks based on OSM data:

* You need to know how to run ``osm2city`` - read its `documentation <https://osm2city.readthedocs.io/en/latest/>`_.
* You need to be aware of the working directory, so you know where the files are going to be written to.
