
Welcome to Hunter's documentation!
==================================

``Hunter`` is a set of services for the free flight simulator `FlightGear <http://home.flightgear.org/>`_ to place different types of targets into `Multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ mode, such that military shooting scenarios can be flown. These services are inspired by the work in the FlightGear flight sim military community `Operation Red Flag (OPRF) <http://opredflag.com/>`_.

The name ``Hunter`` refers to the beautiful British `Hawker Hunter <https://en.wikipedia.org/wiki/Hawker_Hunter>`_, which for many years provided the ground attack capability for the `Swiss Air Force <https://en.wikipedia.org/wiki/Swiss_Air_Force>`_.

``Hunter`` uses FlightGear's `multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ capability, because multiplayer allows several people to see the results (`Nasal <http://wiki.flightgear.org/Nasal_scripting_language>`_ is better suited for single player). And because OPRF-assets are written for multiplayer.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation_server
   end_user
   development



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Credits
=======

The interaction with FlightGear multiplayer is using parts of `ATC-pie <http://wiki.flightgear.org/ATC-pie>`_ by Michael Filhol during execution. Also some of the MP interaction code is directly inspired by code in ATC-pie.

Some data preparation like scenery objects (buildings, roads, etc.) as well as routing data for movable targets like ships are based on `OpenStreetMap (OSM) <https://www.openstreetmap.org/>`_ data being processed with `osm2city <http://wiki.flightgear.org/Osm2city.py>`_.


People
======

**Core Developers**: Rick Gruber-Riemer

**Contributors**: coaching on Discord by OPRF members like ``pinto``, ``Swamp``, ``Richard`` and ``Leto``.


License
=======
This software is licensed under `GNU GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_.
