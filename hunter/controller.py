"""
Controls all aspects of a scenario and runs the mp_targets. fg_targets (instances of FlightGear) run in workers,
but are also controlled by the Controller.

https://docs.python.org/3/library/subprocess.html
https://docs.python.org/3.7/library/multiprocessing.html

"""

import logging
import multiprocessing as mp
import random
import sys
import time
from typing import Any, Dict, List, Optional

from google.cloud import pubsub

import networkx as nx

import hunter.damage as d
import hunter.mp_targets as mpt
import hunter.fg_targets as fgt
import hunter.emesary_notifications as en
import hunter.fgms_io as fio
import hunter.gcp_ds_io as gds
import hunter.gcp_ps_io as gps
import hunter.gcp_utils as gu
import hunter.geometry as g
import hunter.messages as m
import hunter.scenarios as sc
import hunter.utils as u


# http://flightgear.sourceforge.net/getstart-en/getstart-enpa2.html

GCI_TIME_LIMIT = 10  # if request is newer than this, then it is supposed to be a resend and therefore ignored
GCI_SAME_TARGET_LIMIT = 120  # if a request is newer than this, then info for the same target might be displayed
GCI_SIMPLIFIED_INTERCEPT = 5000  # if below value, then intercept vector will be calculated in a simplified way

FGMS_CLIENT_PORT = 5010


class ObjectContainer:
    __slots__ = ('last_position_update', 'last_position_processed')

    def __init__(self) -> None:
        # need to have a fake default position so properties can be accessed immediately
        self.last_position_update = m.PositionUpdate('', '', m.HealthType.fit_for_fight, g.Position(0., 0., 0.))
        self.last_position_processed = time.time()

    @property
    def health(self) -> m.HealthType:
        return self.last_position_update.health

    @health.setter
    def health(self, new_health: m.HealthType) -> None:
        self.last_position_update.health = new_health

    @property
    def position(self) -> g.Position:
        return self.last_position_update.position

    @property
    def heading(self) -> float:
        return self.last_position_update.heading

    @property
    def speed(self) -> float:
        return self.last_position_update.speed

    def process_position(self, current_pos: g.Position, callsign: str, kind: str) -> None:
        """Position object is from ATCpie and is tuple of EarthCoord and alt_ft"""
        current_time = time.time()
        if self.last_position_update is not None:
            # now we can also get speed and bearing
            bearing = g.calc_bearing_pos(self.last_position_update.position, current_pos)
            distance = g.calc_distance_pos(self.last_position_update.position, current_pos)
            speed = distance / (current_time - self.last_position_processed)
            self.last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos, bearing, speed)
        else:
            self.last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos)
        self.last_position_processed = current_time

    def is_zombie(self) -> bool:
        callsign = self.last_position_update.callsign
        zombie = True if time.time() - self.last_position_processed > u.POS_UPDATE_FREQ_MOVING_TARGET * 3 else False
        if zombie:
            logging.debug('%s is zombie at time %i and last_position_processed %i', callsign, time.time(),
                          self.last_position_processed)
        return zombie


class DefenderContainer(ObjectContainer):
    """Container for defenders.
    The defenders can be registered, but actually no pilot flying -> last seen gives an indication."""

    def __init__(self):
        super().__init__()


class MPTargetContainer(ObjectContainer):
    """Container for targets represented by simulated MP instances served by the controller"""
    __slots__ = 'sender'

    def __init__(self, sender: fio.FGMSSender) -> None:
        super().__init__()
        self.sender = sender  # either *Runner or FGMSSender

    @property
    def mp_target(self) -> Optional[mpt.MPTarget]:
        """Returns None if this is not a simulated target, else a pointer to the object."""
        if isinstance(self.sender.mp_target, mpt.MPTarget):
            return self.sender.mp_target
        return None

    @property
    def mp_target_shooter(self):  # can return a ShooterImpl
        """Returns None if this is not a simulated shooting target, else a pointer to the object."""
        if isinstance(self.sender.mp_target, mpt.MPTarget) and self.sender.mp_target.is_shooting:
            return self.sender.mp_target.shooter
        return None

    @property
    def mp_moving_target(self) -> Optional[mpt.MPTargetMoving]:
        """Returns None if this is not a simulated moving target, else a pointer to the object."""
        target = self.mp_target
        if target and isinstance(target, mpt.MPTargetMoving):
            return target
        return None


class FGTargetContainer(ObjectContainer):
    """Container for targets represented by FGFS instances served by a worker over GCP pub/sub"""
    __slots__ = ('worker_id', 'target_type', 'name', 'last_damage_result')

    def __init__(self, worker_id: str) -> None:
        super().__init__()
        self.worker_id = worker_id
        self.target_type = None  # if it is None, then this container is just a placeholder - else mpt.MPTargetType
        self.name = None
        self.last_damage_result = None  # used when real FG instance is destroyed -> result resent

    def last_damage_result_as_destroyed(self) -> Optional[m.DamageResult]:
        """If there has been a damage result registered at all, then resend the last to give the last
        pilot hitting the reward of having destroyed the target."""
        if self.last_damage_result:
            res = self.last_damage_result.copy_with_health(m.HealthType.destroyed)
            self.last_damage_result = None  # in case proxy is reused - e.g. for automat
            return res
        return None

    @property
    def placeholder(self) -> bool:
        return self.target_type is None

    def reset(self) -> None:
        """Set back to being a placeholder and not an FG target."""
        self.target_type = None
        self.name = None

    def make_sam(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.vehicle
        self.name = name

    def make_frigate(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.ship
        self.name = name

    def make_automat(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.plane
        self.name = name

    def make_carrier(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.ship
        self.name = name

    def make_tanker(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.plane
        self.name = name


class AttackerContainer(ObjectContainer):
    __slots__ = ('mp_time_stamp', 'results',
                 '_last_gci_request', '_last_gci_target', '_last_gci_response_type', 'last_emesary_idx', 'fgfs_model',
                 'newest_position', 'newest_position_time')

    def __init__(self, fgfs_model: str) -> None:
        super().__init__()
        self.mp_time_stamp = 0  # MP time is not the same as local computer time!
        self.results = dict()  # key = callsign target, value = List of health results
        self._last_gci_request = 0  # timestamp based on time.time()
        self._last_gci_target = None  # callsign string
        self._last_gci_response_type = None  # m.GCIResponseType
        self.last_emesary_idx = -1
        self.fgfs_model = fgfs_model

        # this position is constantly updated, last_position is not -> datastore
        self.newest_position = None  # g.Position
        self.newest_position_time = 0

    def add_result(self, result: m.DamageResult) -> None:
        if result.target not in self.results:
            self.results[result.target] = list()
        self.results[result.target].append(result.health)

    def results_as_string(self) -> str:
        if self.results:
            result = list()
            for a_target, result_list in self.results.items():
                for health in result_list:
                    result.append('{}: {}'.format(a_target, health.name))
            result_string = '* ' + '\n* '.join(result)
            return result_string
        return 'no hits registered'

    def process_gci_request(self, gci_requests: List[Optional[bool]]) -> Optional[m.GCIResponseType]:
        """Checks request for what type of response is required and returns it if outside time limit - else None. """
        current_time = time.time()
        last_request = -1  # 0, 1, or 2 if a request was made outside time limit and is the newest one
        for idx in range(3):
            if gci_requests[idx] is True:
                if current_time - self._last_gci_request > GCI_TIME_LIMIT:
                    last_request = idx
                    self._last_gci_request = current_time
                break  # only one gci request type can be processed at a time
        if last_request == 0:
            return m.GCIResponseType.picture
        elif last_request == 1:
            return m.GCIResponseType.bogey_dope
        elif last_request == 2:
            return m.GCIResponseType.cutoff
        return None

    def get_last_requested_target(self, response_type: m.GCIResponseType) -> Optional[str]:
        """Returns the callsign of the last target's callsign if within time limit and same response type"""
        current_time = time.time()
        if self._last_gci_target is None:
            return None
        if (current_time - self._last_gci_request < GCI_SAME_TARGET_LIMIT) and \
                self._last_gci_response_type is response_type:
            return self._last_gci_target
        return None

    def set_last_requested_target(self, callsign: str, response_type: m.GCIResponseType) -> None:
        self._last_gci_target = callsign
        self._last_gci_response_type = response_type

    def is_shootable(self) -> bool:
        if self.newest_position is None:
            return False
        # only process if data available during the last few seconds
        delta_time = time.time() - self.newest_position_time
        return delta_time < u.MAX_DELTA_SHOOTABLE


def number_part_of_callsign(callsign: str) -> int:
    """Extracts the last two letters the callsign and converts them to a number."""
    number_part = callsign[-2:]
    logging.debug('Extracted %s as number from callsign % s', number_part, callsign)
    return int(number_part)


class Controller(mp.Process):
    """Partly inspired by APT-pie session.flightgearMP.FlightGearMultiPlayerSessionManager"""
    def __init__(self, mp_server_host: str, parent_conn: mp.Pipe,
                 ctrl_callsign: str, identifier: str, gci_responses: bool,
                 cloud_integration_level: u.CloudIntegrationLevel,
                 scenario_name: str, hostile: bool) -> None:
        super().__init__()

        if ctrl_callsign is None or len(ctrl_callsign) > 7:
            raise ValueError('The callsign for the controller must at least have one character and not more than 7')
        if identifier is None or len(identifier) > 5:
            raise ValueError('The identifier must have between 1 and 5 letters')
        self.scenario = sc.load_scenario(scenario_name)  # raises ValueError is it does not exist
        self.identifier = identifier
        self.callsign = ctrl_callsign
        self.mp_server_host = mp_server_host
        self.hostile = hostile
        # there is no port parameter because there are many assumptions on port ranges -> u.PORT_MP_BASIS

        self.parent_conn = parent_conn  # The Pipe to the parent process (for command line CTRL-C only)
        self.attackers = dict()  # key = callsign, value = AttackerContainer
        self.attackers_hist_positions = dict()  # key = callsign, value = PositionHistory
        self.defenders = dict()  # key = callsigns, value = DefenderContainer

        self.mp_targets = dict()  # key = target_callsign, value = MPTargetContainer
        self.fg_targets = dict()  # key = target_callsign, value = FGTargetContainer (can be None)

        # following handled during set_up() and tear_down()
        self.fgms_receiver = None
        self.number_of_mp_messages_received = 0  # used to do a bit of heart beat control
        self.gci_responses = gci_responses

        self.cloud_integration_level = cloud_integration_level
        self.has_workers = False

        publisher_client = None  # publisher and subscriber are local to init, as run() is another process
        if u.use_datastore(self.cloud_integration_level):
            ds_client = gu.create_datastore_client()
            try:
                if self.cloud_integration_level is u.CloudIntegrationLevel.webui:
                    session = gds.fetch_web_initiated_session(ds_client)
                else:
                    session = gds.register_new_session(ds_client, self.scenario, self.mp_server_host,
                                                       self.gci_responses, self.callsign, self.identifier)
                self.session_id = session[gds.PROP_SESSION_ID]
            except ValueError:
                logging.exception('Cannot start controller because interaction with Cloud is not working as expected')
                sys.exit(1)
        if u.use_pubsub(self.cloud_integration_level):
            publisher_client = gu.construct_publisher_client()
            subscriber_client = gu.construct_subscriber_client()
            self._request_workers(subscriber_client, publisher_client, scenario_name)
        self.gcp_ps_runner = None  # will be set in run(method) if needed
        self.gcp_ds_runner = None  # (ditto)

        # Populate assets
        random.shuffle(self.scenario.static_targets)  # make sure that there is randomness in callsigns etc
        self._populate_assets(publisher_client)

        self.last_next_moving_poll = 0  # when has there last time been checked to add a new moving target?
        self.last_position_processed = 0  # based on u.POS_UPDATE_FREQ_MOVING_TARGET
        self.last_send_position_update_ds = 0  # based on u.SEND_POSITION_UPDATES_TO_DS
        self.last_one_second_poll = 0  # reset every second to be able to make checks every second (run cycle is faster)

        self.run_cycles = 0
        self.last_receiver_heartbeat = 0

    # noinspection PyBroadException
    def _request_workers(self, subscriber: pubsub.SubscriberClient, publisher: pubsub.PublisherClient,
                         scenario_name: str) -> None:
        """Sends out requests to workers to register.
        If no reply, then self.has_workers is set to False, such that that interaction is not done."""
        try:
            gps.handle_workers_to_ctrl_subscription(subscriber, True)
            logging.info('Requesting workers for scenario %s and session ID=%s', scenario_name, self.session_id)
            gps.send_action_request_workers_to_register(publisher, self.session_id, scenario_name,
                                                        self.mp_server_host)
            time.sleep(u.WORKER_POLLING_REGISTRATION_REQUEST * 3)  # give it a bit of time
            logging.info('Checking for answers from workers')
            available_workers = gps.pull_workers_serving(subscriber, self.session_id)
            if available_workers:
                self.has_workers = True
                self._pre_allocate_callsigns_fg_targets(available_workers)
                logging.info('Workers serving: %s', str(available_workers))
            else:
                logging.info('No workers available. Serving only MP targets, no FG instances.')
                self.has_workers = False
        except Exception:
            logging.exception('Something went wrong in requesting workers. Collaboration with workers stopped')
            self.has_workers = False

    def _populate_assets(self, publisher: pubsub.PublisherClient) -> None:
        """Populates all missile assets and all non-moving assets - both FG targets and MP targets"""
        # get callsigns in place
        for defender in self.scenario.defending_callsigns:
            self._add_defending_aircraft(defender)

        if self.has_workers:
            self._populate_carrier(publisher)
            self._populate_tanker(publisher)
            self._populate_automats(publisher)  # next priority, so first
            self._populate_missile_ships(publisher)  # must come before SAMs due to fixed number from scenario
            self._populate_missile_sams(publisher)  # number is dynamic based on remaining capacity in fg_targets

    def _populate_carrier(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with at most one MP carrier."""
        if self.scenario.carrier_definition is None:
            return
        callsign_to_remove = None
        for callsign, container in self.fg_targets.items():
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    my_callsign = callsign
                    if self.scenario.carrier_definition.use_tacan_callsign:
                        my_callsign = fgt.make_tacan_callsign(self.scenario.carrier_definition.carrier_type)
                    carrier_name = self.scenario.carrier_definition.carrier_type.name
                    gps.send_action_add_instance_carrier(publisher, self.session_id, container.worker_id,
                                                         carrier_name, my_callsign)
                    logging.info('Returned from GCP send_action_add_instance_carrier')
                    container.make_carrier(carrier_name)
                    container.last_position_update = m.PositionUpdate('', carrier_name, m.HealthType.fit_for_fight,
                                                                      self.scenario.centre_position, 0)
                    if callsign != my_callsign:
                        self.fg_targets[my_callsign] = container
                        callsign_to_remove = callsign

                except Exception:
                    logging.exception('Could not send action to request a carrier instance')
                break  # no matter whether we succeed below - if it is broken it is broken the first time
        if callsign_to_remove:
            del self.fg_targets[callsign_to_remove]

    def _populate_tanker(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with at most one air-to-air refueler."""
        if self.scenario.tanker_definition is None:
            return
        callsign_to_remove = None
        for callsign, container in self.fg_targets.items():
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    my_callsign = mpt.get_random_tanker_name()
                    gps.send_action_add_instance_tanker(publisher, self.session_id, container.worker_id,
                                                        my_callsign)
                    logging.info('Returned from GCP send_action_add_instance_tanker')
                    container.make_tanker('tanker')
                    container.last_position_update = m.PositionUpdate('', 'tanker', m.HealthType.fit_for_fight,
                                                                      self.scenario.centre_position, 0)
                    if callsign != my_callsign:
                        self.fg_targets[my_callsign] = container
                        callsign_to_remove = callsign

                except Exception:
                    logging.exception('Could not send action to request a tanker instance')
                break  # no matter whether we succeed below - if it is broken it is broken the first time
        if callsign_to_remove:
            del self.fg_targets[callsign_to_remove]

    def _populate_automats(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with automats (shooting fighter aircraft) running in a worker.
        The number of automats is static from the scenario. Depending on capacity in fg_assets all or some
        automats get created."""
        if self.scenario.automats is None or len(self.scenario.automats) == 0:
            return
        remaining_requested = len(self.scenario.automats)
        random.shuffle(self.scenario.automats)
        for callsign, container in self.fg_targets.items():
            if remaining_requested == 0:
                break
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    automat = self.scenario.automats[remaining_requested - 1]
                    props_string = automat.create_properties_string(callsign)
                    gps.send_action_add_instance_automat(publisher, self.session_id, container.worker_id,
                                                         automat.craft.name, callsign,
                                                         self.scenario.icao, automat.number_of_lives, props_string)
                    logging.info('Returned from GCP send_action_add_instance_automat')
                    container.make_automat(automat.craft.name)
                    container.last_position_update = m.PositionUpdate('', automat.craft.name,
                                                                      m.HealthType.fit_for_fight,
                                                                      self.scenario.centre_position, 0)
                    remaining_requested -= 1
                except Exception:
                    logging.exception('Could not send action to request an automat instance')

    def _populate_missile_ships(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with missile ships running in a worker.
        The number of ships is static from the scenario. Depending on capacity in fg_assets all or some
        ships get created."""
        remaining_requested = self.scenario.ships_missile
        for callsign, container in self.fg_targets.items():
            if remaining_requested == 0:
                break
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    gps.send_action_add_instance_ship(publisher, self.session_id, container.worker_id,
                                                      mpt.MISSILE_FRIGATE_NAME, callsign)
                    logging.info('Returned from GCP send_action_add_instance_ship')
                    container.make_frigate(mpt.MISSILE_FRIGATE_NAME)
                    # this is pure random and will get adjusted as soon as the ship sails
                    way_point = random.choice(list(self.scenario.ships_network.nodes))
                    container.last_position_update = m.PositionUpdate('', mpt.MISSILE_FRIGATE_NAME,
                                                                      m.HealthType.fit_for_fight,
                                                                      way_point.make_position(), random.randint(0, 360))
                    remaining_requested -= 1
                except Exception:
                    logging.exception('Could not send action to request a missile ship instance')

    def _populate_missile_sams(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with missile SAMs running in a worker.
        The SAMs in a scenario have a priority assigned: the higher the priority (1 is highest) the more likely
        a static asset will be transformed into a shooting SAM - as long as there is capacity in fg_targets.
        """
        prio_1 = list()
        prio_2 = list()
        prio_3 = list()
        for static_target in self.scenario.static_targets:
            if static_target.target_type in [mpt.MPTarget.BUK_M2, mpt.MPTarget.S_300]:
                if static_target.missile_priority == 0:
                    continue
                elif static_target.missile_priority == 1:
                    prio_1.append(static_target)
                elif static_target.missile_priority == 2:
                    prio_2.append(static_target)
                else:
                    prio_3.append(static_target)

        for callsign, container in self.fg_targets.items():
            if container.placeholder:
                if prio_1:
                    chosen_target = prio_1.pop()
                elif prio_2:
                    chosen_target = prio_2.pop()
                elif prio_3:
                    chosen_target = prio_3.pop()
                else:
                    break  # no point in looping if no priorities left

                # noinspection PyBroadException
                try:
                    gps.send_action_add_instance_sam(publisher, self.session_id, container.worker_id,
                                                     chosen_target.target_type, callsign,
                                                     chosen_target.position.lon, chosen_target.position.lat,
                                                     chosen_target.position.alt_m, chosen_target.heading)
                    logging.info('Returned from GCP send_action_add_instance_sam')
                    container.make_sam(chosen_target.target_type)
                    container.last_position_update = m.PositionUpdate('', chosen_target.target_type,
                                                                      m.HealthType.fit_for_fight,
                                                                      chosen_target.position, chosen_target.heading)
                    self.scenario.static_targets.remove(chosen_target)
                except Exception:
                    logging.exception('Could not send action to request a SAM instance: %s', str(chosen_target))

    # noinspection PyBroadException
    def run(self) -> None:
        """Overrides the method in Process and runs the endless loop"""

        # for some reason configuring GCP logging is also needed within run()
        _ = gu.configure_gcp_logging('INFO', 'Controller')

        logging.info('Starting MPSessionManager processing')

        logging.info('Setting up receiver and start accepting messages')
        self._set_up_receiver()

        if u.use_datastore(self.cloud_integration_level):
            self.gcp_ds_runner = gds.GCPDSRunner(self.session_id)
            self.gcp_ds_runner.start()

        if u.use_pubsub(self.cloud_integration_level):
            self.gcp_ps_runner = gps.GCPPSRunner(self.session_id, None)
            self.gcp_ps_runner.start()

        # add static and towed mp_targets - moving will be added dynamically
        # need to be added in run-methods, such that daemon for FGMSRunner is not stopped
        for static_target in self.scenario.static_targets:
            self._add_static_target(static_target.target_type, static_target.position, static_target.heading)
        self._add_towed_targets()

        self.scenario.stamp_scenario_as_started()

        try:
            while True:
                now = time.time()
                self.run_cycles += 1
                if self.run_cycles % 1000 == 0:
                    logging.info('Cycle %i in Controller', self.run_cycles)
                if self.parent_conn.poll():
                    message = self.parent_conn.recv()
                    logging.info('MPSessionManager got message: %s', message)
                    if isinstance(message, m.ExitSession):
                        self._exit_session()
                        return
                if self.fgms_receiver:
                    # potentially a lot since last main loop, especially because GCP can take time
                    while True:
                        message = self.fgms_receiver.receive_message()
                        if message is None:
                            break  # we are done polling data from GCP right now
                        if isinstance(message, m.ReceivedFGMSData):
                            self._receive_fgms_data(message.udp_package)
                        elif isinstance(message, m.RemoveDeadTarget):
                            logging.warning('FGMS for Controller shot down. No point to continue. Exit.')
                            self._exit_session()
                            return
                        elif isinstance(message, m.HeartbeatReceiver):
                            logging.info('Received heartbeat from Receiver')
                            self.last_receiver_heartbeat = time.time()

                    if self.last_receiver_heartbeat > 0 and \
                            now - self.last_receiver_heartbeat > (5 * mpt.MP_RECEIVER_SEND_FREQ):
                        logging.warning('No heartbeat received from Receiver - re-starting Receiver')
                        self._tear_down_receiver()
                        time.sleep(10)  # this is ca. the zombie time of MP
                        self._set_up_receiver()
                else:
                    logging.warning('No receiver object. Something is wrong in the logic.')
                # messaging from targets' FGMSSenders through Pipes
                to_remove = list()  # cannot remove during iteration
                for target_container in self.mp_targets.values():
                    while True:
                        message = target_container.sender.receive_message()
                        if message is None:
                            break  # we are done polling data from GCP right now
                        if isinstance(message, m.DamageResult):
                            self._register_damage(message)
                        elif isinstance(message, m.PositionUpdate):
                            target_container.last_position_update = message
                        elif isinstance(message, m.RemoveDeadTarget):
                            to_remove.append(message.callsign)

                for callsign in to_remove:
                    del self.mp_targets[callsign]
                if u.use_pubsub(self.cloud_integration_level):
                    while True:
                        data = self.gcp_ps_runner.receive_message()
                        if data is None:
                            break  # we are done polling data from GCP right now
                        logging.info('Receiving stuff from GCPPSRunner through queue: %s', str(data))
                        if data and isinstance(data, m.GCPSubData):
                            if data.content_dict[gps.ACTION] in [gps.ACTION_DEAD, gps.ACTION_CRASH]:
                                worker_id = data.worker_id
                                self.fg_targets[data.content_dict[gps.CALLSIGN]] = FGTargetContainer(worker_id)
                            elif data.content_dict[gps.ACTION] == gps.ACTION_DESTROYED:
                                target_container = self.fg_targets[data.content_dict[gps.CALLSIGN]]
                                if not target_container.placeholder:  # this message could arrive after dead
                                    target_container.health = m.HealthType.destroyed
                                    destroyed_damage = target_container.last_damage_result_as_destroyed()
                                    if destroyed_damage:
                                        self._register_damage(destroyed_damage)
                            else:
                                logging.warning('Programming error: action %s not known here',
                                                data.content_dict[gps.ACTION])

                if u.use_datastore(self.cloud_integration_level):
                    if (now - self.last_send_position_update_ds) > u.SEND_POSITION_UPDATES_TO_DS:
                        self.last_send_position_update_ds = now
                        position_updates = m.PositionUpdatesBatch()
                        for attacker in self.attackers.values():
                            if attacker and attacker.is_zombie() is False:
                                position_updates.add_position_update(attacker.last_position_update)
                        for target in self.mp_targets.values():
                            if target:  # not checking for zombie
                                position_updates.add_position_update(target.last_position_update)
                        for target in self.fg_targets.values():
                            if target and target.is_zombie() is False:
                                position_updates.add_position_update(target.last_position_update)
                        for defender in self.defenders.values():
                            if defender and defender.is_zombie() is False:
                                position_updates.add_position_update(defender.last_position_update)
                        self.gcp_ds_runner.post_message(position_updates)

                # check whether we should dynamically add another moving target
                if (now - self.last_next_moving_poll) > self.scenario.polling_freq:
                    self.last_next_moving_poll = now
                    self._poll_next_moving_targets()
                # just shortly pause the endless loop; must be a low number due to maybe many MP items
                time.sleep(0.01)
                if (now - self.last_one_second_poll) > 1:
                    self.last_one_second_poll = now
                    self._poll_next_trip_targets()
                    if self.hostile:
                        self._process_shooting()

        except Exception:
            logging.exception('Some exception during controller.run() occurred. Exiting the session.')
            # FIXME should use more clear Exception to distinguish what to let go and what to catch
            #  self._exit_session()

    def _exit_session(self):
        logging.info("MPSessionManager got exit command: shutting down dependencies and itself")
        self._tear_down_receiver()
        # MP Targets
        for key, target_container in self.mp_targets.items():
            target_container.sender.post_message(m.TearDownTarget())
        if self.gcp_ps_runner:
            self.gcp_ps_runner.post_message(m.ExitSession())
        if self.gcp_ds_runner:
            self.gcp_ds_runner.post_message(m.ExitSession())
        # FG Targets and workers
        # No need to tear each single FG target down - that is done on worker side
        if self.has_workers:
            publisher_client = gu.construct_publisher_client()  # need to
            subscriber_client = gu.construct_subscriber_client()
            workers = set()
            for container in self.fg_targets.values():
                if container is not None:
                    workers.add(container.worker_id)
            for worker_id in workers:
                gps.send_action_worker_stop(publisher_client, self.session_id, worker_id)
                logging.info('Returned from GCP send_action_worker_stop')
            gps.handle_workers_to_ctrl_subscription(subscriber_client, False)
        # finish up
        self._print_total_stats()
        self.mp_targets.clear()
        self.fg_targets.clear()
        self.attackers.clear()

        # give all threads a bit time to finish up and asynchronously execute exit commands
        time.sleep(20)

        logging.info('FGMS interaction stopped')

    def _print_total_stats(self) -> None:
        """Prints the damage results to sys.out"""
        print('######## DAMAGE STATS ########')
        for callsign, attacker_container in self.attackers.items():
            stats = attacker_container.results_as_string()
            print('Stats for {}: \n{}'.format(callsign, stats))

        print('\n\n######## REMAINING STATS ########')
        remaining_ships = 0
        remaining_helis = 0
        remaining_drones = 0
        remaining_planes = 0
        remaining_towed = 0
        remaining_static = 0
        remaining_vehicles = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_target
            if tgt and tgt.target_type is mpt.MPTargetType.ship:
                remaining_ships += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.helicopter:
                remaining_helis += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.drone:
                remaining_drones += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.plane:
                remaining_planes += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.towed:
                remaining_towed += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.static:
                remaining_static += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.vehicle:
                remaining_vehicles += 1
        print('Remaining ships: {}'.format(remaining_ships))
        print('Remaining helicopters: {}'.format(remaining_helis))
        print('Remaining drones: {}'.format(remaining_drones))
        print('Remaining planes: {}'.format(remaining_planes))
        print('Remaining towed: {}'.format(remaining_towed))
        print('Remaining static: {}'.format(remaining_static))
        print('Remaining vehicles: {}'.format(remaining_vehicles))

    def _poll_next_moving_targets(self) -> None:
        remaining_ships = 0
        remaining_helis = 0
        remaining_drones = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_moving_target
            if tgt and tgt.target_type is mpt.MPTargetType.ship:
                remaining_ships += 1
            if tgt and tgt.target_type is mpt.MPTargetType.helicopter:
                remaining_helis += 1
            if tgt and tgt.target_type is mpt.MPTargetType.drone:
                remaining_drones += 1

        add_message = self.scenario.next_moving_ship(remaining_ships)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
        add_message = self.scenario.next_moving_heli(remaining_helis)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
        add_message = self.scenario.next_moving_drone(remaining_drones)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)

    def _poll_next_trip_targets(self) -> None:
        activated_trip_targets = self.scenario.next_trip_targets()
        if activated_trip_targets:
            for trip_target in activated_trip_targets:
                self._add_trip_target(trip_target)

    def _add_static_target(self, name: str, pos: g.Position, hdg: float = 0.) -> None:
        mp_target = mpt.MPTarget.create_target(name, pos, hdg)
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        send_freq = mpt.MP_MOVING_SEND_FREQ if mp_target.is_shooting else mpt.MP_STATIC_SEND_FREQ
        return self._add_target(mp_target, send_freq)

    def _add_moving_target(self, asset: mpt.MPTargetAsset, path: nx.Graph, start_node: g.WayPoint,
                           override_speed: int = 0) -> None:
        mp_target = mpt.MPTargetMoving.create_moving_target(asset, path, start_node, override_speed)
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        self._add_target(mp_target, mpt.MP_MOVING_SEND_FREQ)

    def _add_trip_target(self, mp_target: mpt.MPTargetTrips) -> None:
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        self._add_target(mp_target, mpt.MP_MOVING_SEND_FREQ)
        logging.info('Added new trip target with activation delay = %i', mp_target.activation_delay)

    def _add_towed_target(self, asset: mpt.MPTargetAsset, path: nx.DiGraph, start_node: g.WayPoint,
                          override_speed: int = 0) -> None:
        towed_target = mpt.MPTargetMoving.create_moving_towed_target(asset, path, start_node, override_speed)
        towed_target.callsign = self._create_callsign(mpt.MPTargetType.towed.name)
        self._add_target(towed_target, mpt.MP_MOVING_SEND_FREQ)

    def _add_towed_targets(self) -> None:
        """Add a series of towed targets.
        Towed targets are just moving targets with a lag, common speed, common start_node and common network.
        The network is directed and simple, such that the tractor and towed target always choose th same.
        There can be more than one tractor/towed combination - they just do not start at the same node.
        """
        if self.scenario.towed_number > 0:
            interval = int(len(self.scenario.towed_network.nodes)/self.scenario.towed_number)
            towed_lag = int(self.scenario.towed_dist / g.knots_to_ms(self.scenario.towed_speed))
            # add tractor towing the asset
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_moving_target(self.scenario.towing_asset, self.scenario.towed_network,
                                        start_node, self.scenario.towed_speed)
            # add distance between tractor and towed by using lag * constant speed
            time.sleep(towed_lag)
            # add the towed target
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_towed_target(self.scenario.towing_asset, self.scenario.towed_network,
                                       start_node, self.scenario.towed_speed)

    def _add_defending_aircraft(self, callsign: str) -> None:
        assert callsign not in self.defenders
        try:
            _ = number_part_of_callsign(callsign)
            if callsign in self.mp_targets:  # need to make place for the callsign by taking another target down
                target_container = self.mp_targets[callsign]
                logging.warning('Taking existing target %s down due to defending aircraft request', callsign)
                target_container.sender.post_message(m.TearDownTarget())
                del self.mp_targets[callsign]

            self.defenders[callsign] = DefenderContainer()
            logging.info('Registered defender %s', callsign)
        except ValueError:
            logging.warning('Not adding {} to defenders due to missing number in callsign.'.format(callsign))

    def _format_callsign(self, type_string: str, index: int) -> str:
        """Target callsign is always 7 letters and either hiding or revealing the type of the target plus 2 digits.
        If revealing: a combination of identifier (min 1 letter) + type (max 4 letters) + index (2 digits)
        If hiding: a 5 letter code + index (2 digits)
        Because the number of targets is limited, but targets are respawned, index is re-used."""
        callsign = self.identifier
        max_type = 5 - len(self.identifier)
        if type_string:
            if len(type_string) <= max_type:
                callsign += type_string
            else:
                callsign += type_string[:max_type]
        else:
            callsign += mpt.MPTargetType.unknown.name[:max_type]
        return callsign + '{:02d}'.format(index)

    def _pre_allocate_callsigns_fg_targets(self, available_workers: Dict[str, int]) -> None:
        """Pre-allocates callsigns for fgfs instance targets, so they are not taken by mp_targets at any time."""
        number = 0
        for capacity in available_workers.values():
            number += capacity
        self.fg_targets = dict()
        pre_allocated_indexes = set()
        for pos in range(number):
            continue_search = True
            while continue_search:
                index = random.randint(u.INDEX_DYNAMIC_START, 99)
                if index not in pre_allocated_indexes:
                    pre_allocated_indexes.add(index)
                    continue_search = False

        for worker_id, capacity in available_workers.items():
            for i in range(capacity):
                if pre_allocated_indexes:
                    container = FGTargetContainer(worker_id)
                    callsign = self._format_callsign('fgfs', pre_allocated_indexes.pop())
                    self.fg_targets[callsign] = container

    def _create_callsign(self, type_string: str) -> str:
        """Finds and allocates the index number of the callsign and then formats it to a valid callsign."""
        next_index = -1
        for digit in range(u.INDEX_DYNAMIC_START, 100):
            no_existing_found = True
            # search in targets' callsigns
            for key in self.fg_targets.keys():
                try:
                    my_digits = number_part_of_callsign(key)
                    if digit == my_digits:
                        no_existing_found = False
                        break
                except ValueError:
                    pass  # happens if there is a carrier or tanker, which does not have 2 digits at the end of callsign
            for key in self.mp_targets.keys():
                my_digits = number_part_of_callsign(key)
                if digit == my_digits:
                    no_existing_found = False
                    break

            if no_existing_found:
                # search in defender's callsign
                for key in self.defenders:
                    my_digits = number_part_of_callsign(key)
                    if digit == my_digits:
                        no_existing_found = False
                        break

            if no_existing_found:
                next_index = digit
                break

        if next_index < 0:
            raise ValueError('No valid index position found - too many targets')

        return self._format_callsign(type_string, next_index)

    def _add_target(self, mp_target: mpt.MPTarget, send_freq: int) -> None:
        fgms_sender = fio.FGMSSender(mp_target, self.mp_server_host, u.PORT_MP_BASIS,
                                     u.PORT_MP_BASIS_CLIENT_CTRL + number_part_of_callsign(mp_target.callsign),
                                     send_freq)
        self.mp_targets[mp_target.callsign] = MPTargetContainer(fgms_sender)
        fgms_sender.start()

    def _set_up_receiver(self) -> None:
        logging.info('Starting Receiver')
        receiver = mpt.MPTarget.create_receiver(self.callsign, self.scenario.centre_position, self.gci_responses)
        self.fgms_receiver = fio.FGMSSender(receiver, self.mp_server_host, u.PORT_MP_BASIS,
                                            u.PORT_MP_BASIS_CLIENT_CTRL + u.INDEX_CONTROLLER,
                                            mpt.MP_RECEIVER_SEND_FREQ)
        self.fgms_receiver.start()
        self.last_receiver_heartbeat = 0

    def _tear_down_receiver(self) -> None:
        if self.fgms_receiver:
            self.fgms_receiver.post_message(m.TearDownTarget())
            self.fgms_receiver = None
        logging.info('Stopped Receiver')

    def _register_damage(self, message: m.DamageResult) -> None:
        logging.info('Message from target: %s', message)
        if message.attacker in self.attackers:
            attacker_container = self.attackers[message.attacker]
            attacker_container.add_result(message)
            if u.use_datastore(self.cloud_integration_level):
                self.gcp_ds_runner.post_message(message)

    def _receive_fgms_data(self, udp_packet) -> None:
        """Interpret data from FGMS. We are only interested in attackers, because they might shoot at targets.

        Callback function used in FGMSSender
        Adapted from ATCpie ext.fgms.update_FgmsAircraft_list and
        session.flightgearMP.FlightGearMultiPlayerSessionManager.receive_fgms_data."""
        self.number_of_mp_messages_received += 1
        if self.number_of_mp_messages_received % 2000 == 0:
            logging.info('Received %i MP messages until now', self.number_of_mp_messages_received)
        try:
            fgms_unpacked_data = fio.decode_fgms_data_packet(udp_packet, self.mp_targets)
        except ValueError as err:
            logging.warning('Ignoring packet: %s' % err)
            return

        if fgms_unpacked_data is None:
            return

        if fgms_unpacked_data.callsign in self.defenders:
            attacker_and_found = False
        elif fgms_unpacked_data.callsign in self.fg_targets:
            attacker_and_found = False
        elif fgms_unpacked_data.callsign in self.attackers:
            attacker_and_found = True
        else:
            self._process_register_attacker(fgms_unpacked_data.callsign, fgms_unpacked_data.fgfs_model)
            attacker_and_found = True

        now = time.time()

        # do attacker specific stuff
        if attacker_and_found:  # process attackers all the time
            container = self.attackers[fgms_unpacked_data.callsign]
            if container.mp_time_stamp > 0 and (
                    container.mp_time_stamp - u.MP_TIMESTAMP_IGNORE_MAXDIFF < fgms_unpacked_data.time_stamp < container.mp_time_stamp):
                return  # Drop unordered UDP packet

            # make updates
            container.mp_time_stamp = fgms_unpacked_data.time_stamp
            container.fgfs_model = fgms_unpacked_data.fgfs_model  # pilot could have changed the plane
            self._process_attacker_gci_requests(fgms_unpacked_data.callsign, fgms_unpacked_data.gci_requests)
            self._process_emesary_notifications(fgms_unpacked_data.callsign, fgms_unpacked_data.encoded_notifications)
            container.newest_position = fgms_unpacked_data.position
            container.newest_position_time = now

        # do the position updates (the ones used for data store etc.
        if (now - self.last_position_processed) > u.POS_UPDATE_FREQ_MOVING_TARGET:
            self.last_position_processed = now
            kind = fgms_unpacked_data.fgfs_model
            if fgms_unpacked_data.callsign in self.fg_targets:
                container = self.fg_targets[fgms_unpacked_data.callsign]
                kind = container.name
                container.process_position(fgms_unpacked_data.position, fgms_unpacked_data.callsign, kind)
            elif fgms_unpacked_data.callsign in self.attackers:
                container = self.attackers[fgms_unpacked_data.callsign]
                container.process_position(fgms_unpacked_data.position, fgms_unpacked_data.callsign, kind)
            elif fgms_unpacked_data.callsign in self.defenders:
                container = self.defenders[fgms_unpacked_data.callsign]
                container.process_position(fgms_unpacked_data.position, fgms_unpacked_data.callsign, kind)
            else:
                logging.warning('%s made it to position update, but is not in a container', fgms_unpacked_data.callsign)

    def _process_register_attacker(self, callsign: str, fgfs_model: str) -> None:
        self.attackers[callsign] = AttackerContainer(fgfs_model)
        logging.info('Registered attacker %s', callsign)

    def _process_emesary_notifications(self, req_callsign: str, encoded_notifications: Dict[int, Any]) -> None:
        """Process incoming Emesary notifications - mostly hit/collision messages.
        """
        if not encoded_notifications:
            return
        attacker_container = self.attackers[req_callsign]
        decoded_notifications = dict()
        for value in encoded_notifications.values():
            if value != '':
                # noinspection PyBroadException
                try:
                    decoded_notifications.update(en.process_incoming_message(value))
                except Exception:
                    logging.error('Cannot process emesary notification for %s', req_callsign)
        for idx, notification in decoded_notifications.items():
            if notification.kind == en.KIND_COLLISION and attacker_container.last_emesary_idx < idx:
                attacker_container.last_emesary_idx = idx
                target_found = False
                # first test the MP targets
                for key in self.mp_targets.keys():
                    if key == notification.remote_callsign:
                        logging.info('Processing armament notification for MP target %s: %s', key, notification)
                        target_container = self.mp_targets[key]
                        target_container.sender.post_message(m.HitNotification(notification, req_callsign,
                                                                               attacker_container.fgfs_model, key))
                        target_found = True
                        break
                # next test the FG targets
                if not target_found:
                    for key, fg_container in self.fg_targets.items():
                        if key == notification.remote_callsign:
                            logging.info('Processing armament notification for FG target %s: %s', key, notification)
                            weapon_type = d.weapon_type_from_notification(notification, fg_container.target_type)
                            health = m.HealthType.hit  # we do not know better
                            fg_container.health = health
                            damage = m.DamageResult(health, weapon_type, req_callsign, attacker_container.fgfs_model,
                                                    key, fg_container.name, False, True)
                            fg_container.last_damage_result = damage
                            self._register_damage(damage)

    def _process_attacker_gci_requests(self, req_callsign: str, gci_requests: List[Optional[bool]]) -> None:
        """Some OPRF planes can request information from GCI. Process the request and reply via MP.

        Documentation: MiG-21bis/Nasal/gci-listeners.nas; oprf-assets/gic-radar/Nasal/gci.nas

        In oprf_assets/GCI-radar/Nasal/gci.nas:
        * func check_requests : message format plus calculation of bearing, range, altitude
            * line 292 ff. PICTURE

        In MiG-21bis:
            * MiG-21-set-common.xml -> bool[0] alias /instrumentation/gci/picture
            * Systems/keyboard.xml: key n=251 (F5) sets /instrumentation/gci/picture

        """
        if not self.gci_responses:
            return
        attacker_container = self.attackers[req_callsign]
        response = attacker_container.process_gci_request(gci_requests)
        if response:
            prev_target_callsign = attacker_container.get_last_requested_target(response)
            next_target_callsign = None
            closest_distance = 9999999
            # mp targets
            for callsign, mpt_container in self.mp_targets.items():
                tgt = mpt_container.mp_target
                if tgt is not None:  # static target is None
                    if mpt_container.health in [m.HealthType.destroyed, m.HealthType.dead]:
                        continue
                    if tgt.civilian:
                        continue
                    elif prev_target_callsign is not None and callsign == prev_target_callsign:
                        next_target_callsign = callsign
                        break
                    elif response is m.GCIResponseType.picture and tgt.target_type is mpt.MPTargetType.ship:
                        distance = g.calc_distance_pos(attacker_container.position, mpt_container.position)
                        if distance < closest_distance:
                            closest_distance = distance
                            next_target_callsign = callsign
                    elif response is m.GCIResponseType.bogey_dope and tgt.target_type is mpt.MPTargetType.vehicle and (
                            self.scenario.gci_allowed_vehicles is None or
                            tgt.name in self.scenario.gci_allowed_vehicles):
                        distance = g.calc_distance_pos(attacker_container.position, mpt_container.position)
                        if distance < closest_distance:
                            closest_distance = distance
                            next_target_callsign = callsign
                    elif response is m.GCIResponseType.cutoff and tgt.target_type in [mpt.MPTargetType.helicopter,
                                                                                      mpt.MPTargetType.drone]:
                        _, eta = g.calc_intercept(attacker_container.position, attacker_container.speed,
                                                  mpt_container.position, mpt_container.speed, mpt_container.heading,
                                                  GCI_SIMPLIFIED_INTERCEPT)
                        if 0 < eta < closest_distance:  # if eta = 0 then cannot be reached by interceptor
                            closest_distance = eta
                            next_target_callsign = callsign

            # fg targets
            if next_target_callsign != prev_target_callsign:
                for callsign, fgt_container in self.fg_targets.items():
                    if not fgt_container.placeholder:
                        if fgt_container.health in [m.HealthType.destroyed, m.HealthType.dead]:
                            continue
                        elif prev_target_callsign is not None and callsign == prev_target_callsign:
                            next_target_callsign = callsign
                            break
                        elif response is m.GCIResponseType.picture and \
                                fgt_container.target_type is mpt.MPTargetType.ship:
                            distance = g.calc_distance_pos(attacker_container.position, fgt_container.position)
                            if distance < closest_distance:
                                closest_distance = distance
                                next_target_callsign = callsign
                        elif response is m.GCIResponseType.bogey_dope and \
                                fgt_container.target_type is mpt.MPTargetType.vehicle:  # we know it is a SAM
                            distance = g.calc_distance_pos(attacker_container.position, fgt_container.position)
                            if distance < closest_distance:
                                closest_distance = distance
                                next_target_callsign = callsign

            if next_target_callsign is None:
                self.fgms_receiver.post_message(m.GCIResponse(m.GCIResponseType.no_info_avail, req_callsign))
            else:
                if next_target_callsign in self.mp_targets:
                    container = self.mp_targets[next_target_callsign]
                else:
                    container = self.fg_targets[next_target_callsign]
                attacker_container.set_last_requested_target(next_target_callsign, response)
                gci_resp = m.GCIResponse(response, req_callsign)
                dist = g.calc_distance_pos(attacker_container.last_position_update.position, container.position)
                gci_resp.set_range(dist)
                altitude = container.position.alt_m
                gci_resp.set_altitude_m(altitude)
                bearing = g.calc_bearing_pos(attacker_container.last_position_update.position, container.position)
                gci_resp.set_bearing(g.calc_magnetic_heading(bearing, self.scenario.magnetic_declination))
                aspect = g.calc_aspect(bearing, container.heading)
                gci_resp.set_aspect(aspect)
                if response is m.GCIResponseType.cutoff:
                    vector, eta = g.calc_intercept(attacker_container.last_position_update.position,
                                                   attacker_container.last_position_update.speed,
                                                   container.position, container.speed, container.heading,
                                                   GCI_SIMPLIFIED_INTERCEPT)
                    gci_resp.set_vector(g.calc_magnetic_heading(vector, self.scenario.magnetic_declination))
                    gci_resp.set_eta_s(eta)
                self.fgms_receiver.post_message(gci_resp)
                if response is m.GCIResponseType.picture:
                    self.fgms_receiver.post_message(m.GCIResponse(m.GCIResponseType.all_sent, req_callsign))
        # else do nothing

    def _process_shooting(self) -> None:
        """Check whether shooting targets have something to do."""
        # prepare aircraft data
        for attacker_callsign, attacker_container in self.attackers.items():
            if attacker_container.is_shootable():  # not set at very startup
                if attacker_callsign in self.attackers_hist_positions:
                    position_history = self.attackers_hist_positions[attacker_callsign]
                else:
                    position_history = g.PositionHistory(u.MAX_DELTA_SHOOTABLE + 1)
                    self.attackers_hist_positions[attacker_callsign] = position_history
                position_history.add_position(attacker_container.newest_position)
            else:  # we need to remove the data
                if attacker_callsign in self.attackers_hist_positions:
                    del self.attackers_hist_positions[attacker_callsign]

        # make sure all attacker history is actually in attackers (should always be the case - but you never know
        for callsign in self.attackers_hist_positions.keys():
            if callsign not in self.attackers:
                del self.attackers_hist_positions[callsign]

        # now we have the data, which the shooters can use
        for mp_target_container in self.mp_targets.values():
            shooter = mp_target_container.mp_target_shooter
            if shooter:
                shooter.process_attackers(self.attackers_hist_positions)
