"""See documentation in emesary.py

Migrated from
 * FGData/Nasal/notification.nas (commit 21675f, 2020-10-19)
 * f-14b/Nasal/ArmamentNotification.nas (commit 9ab6240, 2020-10-17)
"""
import logging
from typing import Dict
import unittest

import hunter.emesary as e
import hunter.geometry as g

GeoEventNotification_Id = 18
ArmamentNotification_Id = 19  # according to F14b. According to notification.nas it would be 22
ArmamentInFlightNotification_id = 21  # not yet implemented in Hunter from f-14b/Nasal/ArmamentNotification.nas
StaticNotification_Id = 25  # not yet implemented in Hunter from f-14b/Nasal/ArmamentNotification.nas

# Defined kinds. Called kind to avoid confusion with notification type (notifications.nas line 237 ff)
KIND_CREATED = 1
KIND_MOVED = 2
KIND_DELETED = 3
KIND_COLLISION = 4


# Secondary kind (8 bits)
# using the first 4 bits as the classification and the second 4 bits as the sub-classification
# All secondary kinds are defined in notifications.nas line 246 ff.
# Here only those secondary kinds get defined, which are needed in Hunter


class KindNotification(e.Notification):
    __slots__ = ('kind', 'secondary_kind', 'remote_callsign')

    def __init__(self, type_: str, notification_id: int, ident: str = 'none',
                 kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(type_, ident, notification_id)
        self.kind = kind  # the activity that the notification represents (KIND_CREATED etc.)
        self.secondary_kind = secondary_kind  # the entity on which the activity is being performed.
        self.remote_callsign = ''


class GeoEventNotification(KindNotification):
    __slots__ = ('name', 'position', 'unique_index', 'flags')

    def __init__(self, ident: str = 'none', name: str = '', kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(GeoEventNotification.__name__, GeoEventNotification_Id, ident,
                         kind, secondary_kind)
        self.name = name  # name of the notification, bridged.
        self.position = None  # geometry.py -> Position
        self.unique_index = 0
        self.is_distinct = 0
        self.flags = 0

    @property
    def bridge_message_notification_key(self) -> str:
        return '{}.{}.{}'.format(self.notification_type, self.ident, self.unique_index)

    # TODO: bridgeProperties


class ArmamentNotification(KindNotification):
    """ArmamentNotification from f-14/Nasal/ArmamentNotification.nas.

    The decoding code from Nasal is in method process_incoming_message()

    Example from F-14 for cannon:
    Kind=4 SecondaryKind=20 RelativeAltitude=-76.299 Distance=2 Bearing=307.5 RemoteCallsign=Richard

    Missile F-14 fox2.nas:
                    var msg = notifications.ArmamentNotification.new("mhit", 4, 21+me.ID);
                    msg.RelativeAltitude = explosion_coord.alt() - me.t_coord.alt();
                    msg.Bearing = explosion_coord.course_to(me.t_coord);
                    msg.Distance = direct_dist_m;
                    msg.RemoteCallsign = me.callsign;
                    f14.hitBridgedTransmitter.NotifyAll(msg);
    Cannon F-14 weapons.nas:
                  var msg = notifications.ArmamentNotification.new("mhit", 4, 151+armament.shells[typeOrd][0]);
                  msg.RelativeAltitude = 0;
                  msg.Bearing = 0;
                  msg.Distance = hits_count;
                  msg.RemoteCallsign = hit_callsign;
                  f14.hitBridgedTransmitter.NotifyAll(msg);

    """
    __slots__ = ('relative_altitude', 'distance', 'bearing')

    def __init__(self, ident: str = 'none', kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(ArmamentNotification.__name__, ArmamentNotification_Id, ident,
                         kind, secondary_kind)
        self.relative_altitude = 0.
        self.is_distinct = 0
        self.distance = 0.
        self.bearing = 0.

    def __str__(self):
        string = 'ArmamentNotification: ident={}, kind={}, secondary kind={}'.format(self.ident, self.kind,
                                                                                     self.secondary_kind)
        string += ', distance={}, bearing={}'.format(self.distance, self.bearing)
        return string

    @classmethod
    def decode_from_message_body(cls, msg_body: bytes) -> 'ArmamentNotification':
        kind, pos = e.TransferByte.decode(msg_body, 0)
        secondary_kind, pos = e.TransferByte.decode(msg_body, pos)
        relative_altitude, pos = e.TransferFixedDouble.decode(msg_body, 2, 1/10, pos)
        distance, pos = e.TransferFixedDouble.decode(msg_body, 2, 1/10, pos)
        bearing, pos = e.TransferFixedDouble.decode(msg_body, 1, 1.54, pos)
        bearing = g.norm_deg_180(bearing)
        remote_callsign, _ = e.TransferString.decode(msg_body, pos)
        notification = ArmamentNotification(ident='mp-bridge', kind=kind, secondary_kind=secondary_kind)
        notification.distance = distance
        notification.bearing = bearing
        notification.remote_callsign = remote_callsign
        return notification

    def encode_to_message_body(self) -> bytes:
        message_body = b''
        message_body += e.TransferByte.encode(self.kind)
        message_body += e.TransferByte.encode(self.secondary_kind)
        message_body += e.TransferFixedDouble.encode(self.relative_altitude, 2, 1/10)
        message_body += e.TransferFixedDouble.encode(self.distance, 2, 1/10)
        message_body += e.TransferFixedDouble.encode(g.norm_deg_180(self.bearing), 1, 1.54)
        message_body += e.TransferString.encode(self.remote_callsign)
        return message_body


# ========= Stuff from emesary_mp_bridge.nas ======================

# See Notes section in documentation at the top of emesary_mp_bridge.nas
SEPARATOR_CHAR = b'!'

MESSAGE_END_CHAR = b'~'


def process_incoming_message(encoded_val: bytes) -> Dict[int, ArmamentNotification]:
    """Processes an incoming Emesary bridge base value and extracts the encoded ArmamentNotifications.

    Mimics new_class.ProcessIncoming = func(encoded_val) ...

    A dict is returned with the message index as key and the notification as value.
    0, 1 or n notifications might be included."""
    notifications = dict()
    if encoded_val == b'':
        return notifications

    encoded_notifications = encoded_val.split(MESSAGE_END_CHAR)
    for encoded_notification in encoded_notifications:
        if encoded_notification == b'':
            continue
        # get the message parts
        encoded_notification_parts = encoded_notification.split(SEPARATOR_CHAR)
        print('encoded parts:', encoded_notification_parts)
        if len(encoded_notification_parts) < 4:
            logging.warning('Error: emesary.IncomingBridge.ProcessIncoming bad msg %s', encoded_notification)
        else:
            msg_idx = e.BinaryAsciiTransfer.decode_int(encoded_notification_parts[1], 4, 0)[0]
            msg_type_id = e.BinaryAsciiTransfer.decode_int(encoded_notification_parts[2], 1, 0)[0]
            msg_body = encoded_notification_parts[3]
            # the original code in Nasal does reflection here. We go with specific code due to clarity
            if msg_type_id == ArmamentNotification_Id:
                try:
                    # decoding based on ArmamentNotification.nas in F-14
                    notification = ArmamentNotification.decode_from_message_body(msg_body)
                    notifications[msg_idx] = notification
                    print(notification)
                except ValueError:
                    logging.exception('Unable to process body of ArmamentNotification.')
    return notifications


# ======== Own stuff Hunter ========


# ------------- Testing --------------
"""
    print('========= F-14 old before bullets ===========')
    data = b'FGFS\x00\x01\x00\x01\x00\x00\x00\x07\x00\x00\x01\x14d\x00\x00\x00\x00\x00\x00\x00HB-VANO\x00Aircraft/f-14b/Models/f-14b.xml\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\xe86\xfa\x8b\x9e\x06\x04?\xb9\x99\x99\x99\x99\x99\x9aA>+)\xd4\x18CeA,\x1c\xf5\xd3\xe0\x8e:AV\xc9{\xf1G\x8dd\xbd\xb69\xd9\xc05\xe9\xb9>w\xa8\x8b5+M\xcc\xb5\x03\x91\xb2\xb5s2\x89\xb4z\xea^\xb4\xa5\xc7E1mw\xb1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xac\xe0\x02\x05\xdc\x04\xb0\x00\x00\x05\xdd\xff\xff\xd8\xf1\x05\xde\x00\x00\x05\xdf\x00\x01\x05\xe0\x00\x00\x05\xe1\xd8\xf1.\xd6\x00\x01\x00\x00.\xf2\x00\x00\x00\x002\xc8\x02\x02'
    values_f14_without = decode_fgms_data_packet(data)
    emesary_dict = values_f14_without[-1]
    decoded_notifications = dict()
    for value in emesary_dict.values():
        if value != '':
            decoded_notifications.update(en.process_incoming_message(value))

    data = b'FGFS\x00\x01\x00\x01\x00\x00\x00\x07\x00\x00\x01*d\x00\x00\x00\x00\x00\x00\x00HB-VANO\x00Aircraft/f-14b/Models/f-14b.xml\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\xe87@\xf2\x04l\xa8?\xb9\x99\x99\x99\x99\x99\x9aA>+)\xd4\x181\x99A,\x1c\xf5\xd3\xe0\x8d\xbdAV\xc9{\xf1G\x9a\x1e\xbd\xb69\xd7\xc05\xe9\xbb>w\xa8\x7f5\x14/\xf2\xb3\xb1\x0b\xa7\xb5\xd5\xf7\x96\xb3$ 7\xb4\x93a10Lf\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xac\xe0\x02\x05\xdc\x04\xb0\x00\x00\x05\xdd\xff\xff\xd8\xf1\x05\xde\x00\x00\x05\xdf\x00\x01\x05\xe0\x00\x00\x05\xe1\xd8\xf1.\xd6\x00\x01.\xf2\x00\x1a!   \x17!\x14!\x05\x15- , \xba V\x08xbuil39~2\xc8\x02\x02'
    values_f14_bullets = decode_fgms_data_packet(data)

    emesary_dict = values_f14_bullets[-1]
    decoded_notifications = dict()
    for value in emesary_dict.values():
        if value != '':
            decoded_notifications.update(en.process_incoming_message(value))

{12018: b'!   \x17!\x14!\x05\x15- , \xba V\x08xbuil39~'}

msg_idx:
b'   \x17'
-1891370986.0

msg_type_id:
b'\x14'
-105.0

========
F-16
========

encoded notification: b'!\x83\x01\x01\r!\x96!\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53'
encoded parts: [b'', b'\x83\x01\x01\r', b'\x96', b'\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53']
msg idx encoded: b'\x83\x01\x01\r'
msg idx decoded: 12.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.1, bearing=30.8
Controller-1 MainThread -- 00:18:08 - INFO     : Processing armament notification for MP target OPFOR53: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.1, bearing=30.8


encoded notification: b'!\x83\x01\x01\x0e!\x96!\x87w\x83\x01\x83\x93\x83\x8aOPFOR50'
encoded parts: [b'', b'\x83\x01\x01\x0e', b'\x96', b'\x87w\x83\x01\x83\x93\x83\x8aOPFOR50']
msg idx encoded: b'\x83\x01\x01\x0e'
msg idx decoded: 13.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=14.0, bearing=0.0
Controller-1 MainThread -- 00:18:30 - INFO     : Processing armament notification for MP target OPFOR50: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=14.0, bearing=0.0


encoded notification: b'!\x83\x01\x01\x0f!\x96!\x87w\x83\x01\x83}\x83\x8aOPFOR50'
encoded parts: [b'', b'\x83\x01\x01\x0f', b'\x96', b'\x87w\x83\x01\x83}\x83\x8aOPFOR50']
msg idx encoded: b'\x83\x01\x01\x0f'
msg idx decoded: 14.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=12.0, bearing=0.0
Controller-1 MainThread -- 00:18:32 - INFO     : Processing armament notification for MP target OPFOR50: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=12.0, bearing=0.0


encoded notification: b'!\x83\x01\x01&!\x98!\x87\x9b\xc9\xc3\x84\x9af\x9f\x83\x06;\x85\xcc\x02\xef\x80\x83\x83\x1f'
encoded parts: [b'', b'\x83\x01\x01&', b'\x98', b'\x87\x9b\xc9\xc3\x84\x9af\x9f\x83\x06;\x85\xcc\x02\xef\x80\x83\x83\x1f']
msg idx encoded: b'\x83\x01\x01&'
msg idx decoded: 33.0
msg_type_id encoded: b'\x98'
msg_type_id decoded: 21.0
Controller-1 MainThread -- 00:20:04 - INFO     : Processing armament notification for MP target OPFOR54: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.0, bearing=93.94


encoded notification: b'!\x83\x01\x01\x11!\x96!\x87w\x83\x01\x84\x8b\x83\x8aOPFOR51'
encoded parts: [b'', b'\x83\x01\x01\x11', b'\x96', b'\x87w\x83\x01\x84\x8b\x83\x8aOPFOR51']
msg idx encoded: b'\x83\x01\x01\x11'
msg idx decoded: 16.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=38.0, bearing=0.0
Controller-1 MainThread -- 00:20:54 - INFO     : Processing armament notification for MP target OPFOR51: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=38.0, bearing=0.0

"""


class TestEmesary(unittest.TestCase):
    def test_numeric_decode_encode(self):
        # int values
        test_tuples = [  # int_value, bytes value, message, length
            (-105, b'\x14', 'F-14 type_id -105', 1),
            (19, b'\x96', 'F-16 decode type_id 19', 1),
            (21, b'\x98', 'F-16 decode type_id 21', 1),
            (-1891370986, b'   \x17', 'F-14 decode index', 4),
            (14, b'\x83\x01\x01\x0f', 'F-16 decode index 14', 4),
            (16, b'\x83\x01\x01\x11', 'F-16 decode index 16', 4),
            (33, b'\x83\x01\x01&', 'F-16 decode index 33', 4)
        ]
        for test_tuple in test_tuples:
            encoded = e.BinaryAsciiTransfer.encode_int(test_tuple[0], test_tuple[3])
            self.assertEqual(test_tuple[1], encoded, test_tuple[2])
            decoded, _ = e.BinaryAsciiTransfer.decode_int(test_tuple[1], test_tuple[3], 0)
            self.assertAlmostEqual(test_tuple[0], decoded, 1, test_tuple[2])

        self.assertAlmostEqual(KIND_COLLISION, e.TransferByte.decode(e.TransferByte.encode(KIND_COLLISION), 0)[0], 1,
                               'Collision type encode-decode')

        orig_message_body = b'\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53'
        notification = ArmamentNotification.decode_from_message_body(orig_message_body)
        self.assertAlmostEqual(KIND_COLLISION, notification.kind, 1, 'F-16 missile kind')
        self.assertAlmostEqual(73, notification.secondary_kind, 1, 'F-16 missile secondary kind')
        self.assertTrue(notification.secondary_kind > 20, 'F-16 it is a warhead')
        self.assertEqual('OPFOR53', notification.remote_callsign, 'F-16 missile remote callsign')

        message_body = notification.encode_to_message_body()
        self.assertEqual(orig_message_body, message_body, 'F-16 missile encoded back')

        orig_message_body = b'\x87w\x83\x01\x83\x93\x83\x8aOPFOR50'
        notification = ArmamentNotification.decode_from_message_body(orig_message_body)
        self.assertTrue(notification.secondary_kind < 0, 'F-16 it is a cannon hit')
        message_body = notification.encode_to_message_body()
        self.assertEqual(orig_message_body, message_body, 'F-16 cannon encoded back')
