from enum import IntEnum, unique
import logging
import os.path as osp
import pickle
import random
from typing import List, Optional, Tuple
import sys
import time

import networkx as nx

import hunter.fg_targets as fgt
import hunter.mp_targets as mpt
import hunter.geometry as g
import hunter.messages as m
import hunter.utils as u


class Airport:
    """An airport definition usable for automats and FGTargets"""
    def __init__(self, icao: str, name: str, runway: str, heading_magnetic_deg: float,
                 ground_elev_ft: float,
                 longitude_deg: float, latitude_deg: float,
                 beacons: List[g.WayPoint]) -> None:
        self.icao = icao
        self.name = name
        self.runway = runway
        self.heading_magnetic_deg = heading_magnetic_deg  # from /orientation
        self.ground_elev_ft = ground_elev_ft  # from /position
        self.longitude_deg = longitude_deg  # from /position
        self.latitude_def = latitude_deg  # from /position
        self.beacons = beacons
        if not self.beacons:
            raise ValueError('Departure beacons needs to have at least 1 item.')


LSMM = Airport('LSMM', 'Meiringen', '28', 274.16, 1897.94, 8.122357102, 46.74124602,
               [g.WayPoint(1000, 8.01, 46.75), g.WayPoint(1001, 7.8, 46.665), g.WayPoint(1002, 7.635, 46.715)])
ENAT = Airport('ENAT', 'Alta', '29', 289.43, -0.2, 23.389255, 69.972425,
               [g.WayPoint(1000, 23.075, 70.037), g.WayPoint(1001, 23., 70.2)])
# Tromsø for tanker
ENTC = Airport('ENTC', 'Tromsø Airport, Langnes', '01', 2.76, 15.65, 18.913071, 69.672764,
               [g.WayPoint(1000, 19.03, 69.75), g.WayPoint(1001, 19.86, 69.875), g.WayPoint(1002, 20., 70.)])

# LIPA for tanker Croatia
LIPA = Airport('LIPA', 'Aviano Air Base', '23', 225.23, 405.76, 12.6086007, 46.03870907,
               [g.WayPoint(1000, 12.5133, 45.800236),  # D191N
                g.WayPoint(1001, 12.45585556, 45.62141667),  # D191Y
                g.WayPoint(1002, 12.281389, 45.071806),  # CHI
                g.WayPoint(1003, 12.713333, 44.542500),  # BELOV
                g.WayPoint(1004, 13.463056, 44.416389)  # BAXON
                ])


APT_DAT_AIRPORT_FILE = 'airports.pkl'  # situated in directory scenario_resources and created by airports_io.py


class AptDatAirport:
    """Simplified airport to hold the minimal data from FG apt-dat.

    According to the x-plane definition 1 = land airport, 16 = sea base, 17 = heliport
    """
    __slots__ = ('icao', 'name', 'lon', 'lat', 'kind')

    def __init__(self, icao: str, name: str, lon: float, lat: float, kind: int) -> None:
        self.icao = icao
        self.name = name
        self.lon = lon
        self.lat = lat
        self.kind = kind


class StaticTarget:
    """A container for the properties of a static target.
    Missile_priority determines whether it might be run as an FG instance of the same type.
    0 means never be run as MP target.
    Other priorities can only be 1 (highest), 2 and 3.
    """
    __slots__ = ('target_type', 'position', 'heading', 'missile_priority')

    def __init__(self, target_type: str, position: g.Position, heading: float, missile_priority: int = 0) -> None:
        self.target_type = target_type
        self.position = position
        self.heading = heading
        self.missile_priority = missile_priority if 0 <= missile_priority < 4 else 3


@unique
class AutomatType(IntEnum):
    """Needs to follow the numbering in automat-set.xml"""
    f_16 = 0
    mig_29 = 1
    f_15 = 2
    su_27 = 3


class AutomatTarget:
    """A container to define an automat (shooting fighter jet on FG instance).

    The parameters match to a certain degree the capabilities of the automat (at beginning of automat-set.xml,
    as per 2020-05-23).
    Parameters mean the following:
    * plan: flight plan (must exist in sub-folder "Routes")
    * craft: type of aircraft (see AutomatType)
    * randomize_plan: if True then randomize the order of waypoints after the 3rd, but not the last
    * fire-first: whether the automat fires first (offensive) or only starts offensive if having been shot at
    * short_range_ammo_max: 0 if None, maximum of aircraft if True, randomized between 1 and max-1 (sidewinder)
    * long_range_ammo_max: ditto (AMRAAM)
    * cannon_ammo: 0 - 150 bullets
    * floor: minimum altitude (ft) if automate is engaged in combat - it will always try to fight above this altitude.
             NB: this can be different from the altitude used in the waypoints of the flight plan
    * floor_dive: Set floor-dive to something above highest mountain peaks.
                  Otherwise it might hit terrain when doing a split-S (because terrain radar will not help in due time).
    * start_speed: Ground speed in KT. If starting from start of runway (cf. plan) it must be set to 0
    * transponder: if transponder is set to False, it will make it tougher to find the automat in a dogfight
    * engage_range: within this distance the automat will engage in fighting
    * g_endurance_modern: set to True means that the pilot uses a modern g-suit (e.g 1997 checkbox  in F-16)
    * pause_after_crash: time in minutes before coming back online. 25 seconds will always be added to this.
    * wait_before_online: time in minutes (after a crash it takes pause_after_crash+wait_before_online)
    * number_of_lives: unlimited if 0, otherwise after a number of crashes the Automat is not restarted anymore
    * tacview: stores https://www.tacview.net/ data into $fghome/Export

    The following property available in the automat is set by the Controller at runtime:
    * forced-callsign
    * forced-port

    The following properties available in the automat are fixed:
    * callsign-override (true)
    * blufor (false)
    * bomber (false)
    * remember-aggression-minutes (10)
    * fuel-percent (100)
    * cruise-mach (0.8)
    * cruise-min-mach (0.6)
    * emergency (false)
    * ceiling (45000)
    * roll (30)
    * counter (2)
    * port-override (true)
    * server-oprf (true)
    """
    __slots__ = ('plan', 'craft', 'randomize_plan', 'fire_first',
                 'a9_ammo', 'a120_ammo', 'cannon_ammo',
                 'floor', 'floor_dive', 'start_speed', 'transponder', 'engage_range',
                 'g_endurance', 'pause_time_minutes', 'freeze_time_minutes', 'number_of_lives', 'tacview')

    def __init__(self, plan: str, craft: AutomatType, randomize_plan: bool, fire_first: bool,
                 short_range_ammo_max: Optional[bool], long_range_ammo_max: Optional[bool], cannon_ammo: int,
                 floor: int, floor_dive: int, start_speed: int, transponder: bool, engage_range: int,
                 g_endurance_modern: bool, pause_after_crash: int, wait_before_online: int,
                 number_of_lives: int = 0, tacview: bool = False) -> None:
        self.plan = plan
        self.craft = craft
        self.randomize_plan = randomize_plan
        self.fire_first = fire_first
        self.a9_ammo = 0  # short_range_ammo_max is None
        if short_range_ammo_max is True:
            self.a9_ammo = 2
        elif short_range_ammo_max is False:
            self.a9_ammo = 1
        self.a120_ammo = 0  # long_range_ammo_max is None
        max_a120 = 4 if self.craft in [AutomatType.f_16, AutomatType.mig_29] else 6
        if long_range_ammo_max is True:
            self.a120_ammo = max_a120
        elif long_range_ammo_max is False:
            self.a120_ammo = random.randint(1, max_a120 - 1)
        self.cannon_ammo = cannon_ammo if 0 < cannon_ammo < 151 else 150
        self.floor = floor
        self.floor_dive = floor_dive
        self.start_speed = start_speed if start_speed >= 0 else 0
        self.transponder = transponder
        self.engage_range = engage_range if 0 < engage_range < 100 else 50
        self.g_endurance = 30 if g_endurance_modern else 10
        self.pause_time_minutes = pause_after_crash if pause_after_crash >= 0 else 0
        self.freeze_time_minutes = wait_before_online if wait_before_online >= 0 else 0
        self.number_of_lives = number_of_lives
        self.tacview = tacview

    def create_properties_string(self, forced_callsign: str) -> str:
        props_string = list()
        props_string.append(u.create_prop_for_string('plan', self.plan))
        props_string.append(u.create_prop_for_bool('callsign-override', True))
        props_string.append(u.create_prop_for_string('forced-callsign', forced_callsign))
        props_string.append(u.create_prop_for_bool('port-override', True))
        props_string.append(u.create_prop_for_bool('server-oprf', True))
        props_string.append(u.create_prop_for_bool('blufor', False))
        props_string.append(u.create_prop_for_bool('fire-first', self.fire_first))
        props_string.append(u.create_prop_for_int('craft', self.craft.value))
        props_string.append(u.create_prop_for_bool('randomize-plan', self.randomize_plan))
        props_string.append(u.create_prop_for_int('a9-ammo', self.a9_ammo))
        props_string.append(u.create_prop_for_int('a120-ammo', self.a120_ammo))
        props_string.append(u.create_prop_for_int('floor', self.floor))
        props_string.append(u.create_prop_for_int('floor-dive', self.floor_dive))
        props_string.append(u.create_prop_for_int('start-speed', self.start_speed))
        props_string.append(u.create_prop_for_int('cannon-ammo', self.cannon_ammo))
        props_string.append(u.create_prop_for_bool('transponder', self.transponder))
        props_string.append(u.create_prop_for_bool('bomber', False))
        props_string.append(u.create_prop_for_int('remember-aggression-minutes', 10))
        props_string.append(u.create_prop_for_int('engage-range', self.engage_range))
        props_string.append(u.create_prop_for_int('fuel-percent', 100))
        props_string.append(u.create_prop_for_double('cruise-mach', 0.8))
        props_string.append(u.create_prop_for_double('cruise-min-mach', 0.6))
        props_string.append(u.create_prop_for_bool('emergency', False))
        props_string.append(u.create_prop_for_int('ceiling', 45000))
        props_string.append(u.create_prop_for_int('G-endurance', self.g_endurance))
        props_string.append(u.create_prop_for_int('roll', 30))
        props_string.append(u.create_prop_for_float('counter', 2.))
        props_string.append(u.create_prop_for_double('pause-time-minutes', self.pause_time_minutes))
        props_string.append(u.create_prop_for_double('freeze-time-minutes', self.freeze_time_minutes))
        props_string.append(u.create_prop_for_bool('tacview', self.tacview))
        return ' '.join(props_string)


class CarrierDefinition:
    """Defines the properties for one carrier.

    Description of parameters:
    * carrier_type: one of the (4) MP carriers - most often Vinson is the best choice due to size and having fleet
    * sail_area: a list of lon/lat confining the area where the carrier can sail (first != last node -> not closed)
                 The sail area should be a https://en.wikipedia.org/wiki/Convex_polygon and at least have one line
                 between 2 points, which is longer than 20 km. Remember that a carrier's turn radius is measured in
                 several kilometres and that it can steam at over 20 kts.
    * loiter_centre: a point around which the carrier could loiters if nothing to do - is also the start point and
                     often in the middle
    * use_tacan_callsign: use the TACAN (as defined in CarrierType.value) instead of a randomised callsign
    """
    __slots__ = ('carrier_type', 'sail_area', 'loiter_centre', 'use_tacan_callsign')

    def __init__(self, carrier_type: fgt.CarrierType, sail_area: List[Tuple[float, float]],
                 loiter_centre: Tuple[float, float], use_tacan_callsign: bool = True) -> None:
        self.carrier_type = carrier_type
        self.sail_area = sail_area
        self.loiter_centre = loiter_centre
        self.use_tacan_callsign = use_tacan_callsign
        if len(self.sail_area) < 3:
            raise ValueError('The sail area for the carrier must at least have 3 nodes')


class TankerDefinition:
    """Defines the properties for one tanker

    Typically the pattern network is at FL200.
    The airport definition includes waypoints out of the airport, after which the tanker just flies towards the closest
    point in the pattern network.
    """
    __slots__ = ('airport', 'tanker_network')

    def __init__(self, airport: Airport, network: nx.Graph) -> None:
        self.airport = airport
        self.tanker_network = network


class ScenarioContainer:
    """Container for all elements of a scenario.

    Description of Parameters:
    * ident: the identifier used in the _build_scenario_* method
    * name: the name of the scenario. Used in saving damage results from pilots hitting an asset
    * description: not used yet.
    * defending_callsigns: list of pilots working as OPFOR and therefore not attacked by assets
    * south_west: the South West corner of the scenario area (mostly for mapping purposes -> should include all
                  airports from which attackers or targets start/land -> "war area")
    * north_east: the North East corner of the scenario area
    * icao: "typical" airport in the scenario - default for automats, but they might take off from another place.
    * airports: can be loaded with the corresponding method from a pickled file based on FG apt-dat info and selected
                based on whether their location is within the south_west / north_east boundary
    * magnetic_declination: used for GCI (https://www.ngdc.noaa.gov/geomag/calculators/magcalc.shtml#declination)
    * gci_allowed_vehicles: None or list of names. If None, then all targets of type vehicle are used in GCI, otherwise
                            only those, whose names are in the list (e.g. [mpt.MPTarget.S_300, mpt.MPTarget.BUK_M2])
    * polling_freq: time in seconds to test whether a new heli or ship should be added

    Most targets are added with add_* methods. See the related description of the functions for parameters.
    NB: targets can only added as part of the scenario creation - not dynamically afterwards.
    And only once (i.e. you cannot add several automats by calling the method add_automats() several times, you need
    to have a final list of automats when calling the method.

    Depending on how many targets the worker node(s) of hunter can run, the parameters above are influenced as
    follows:
    * the carrier (only one) has the top priority
    * the tanker (only one) has the next priority
    * automats have next priority - as long as there is space on worker(s) they will be present
    * missile ships have second highest priority - ditto
    * if there is still space on worker(s), then randomly SAMs from static targets will be replaced by shooting SAMs

    E.g. there are 2 automats defined, ships_missile=2 and 1 carrier. In static targets there are 15 SAMs defined
    with different priorities. Now if Hunter is run with 7 slots for shooting stuff, then 2+2+1 slots would be taken
    by automats/ships/carrier. The 2 remaining slots would be randomly used to fill with shooting SAMs out of
    the 15 defined by priority (see 'missile_priority' in StaticTarget).
    """

    def __init__(self, ident: str, name: str, description: str, defending_callsigns: List[str],
                 south_west: Tuple[float, float], north_east: Tuple[float, float],
                 icao: str,
                 magnetic_declination: float = 0,
                 gci_allowed_vehicles: Optional[List[str]] = None,
                 polling_freq: int = 60) -> None:
        self.ident = ident
        self.name = name
        self.description = description
        self.defending_callsigns = defending_callsigns
        self.static_targets = list()
        self.automats = None

        self.ships_missile = 0
        self.ships_min = 0
        self.ships_initial = 0
        self.ships_network = None
        self.ship_assets = list()

        self.carrier_definition = None

        self.tanker_definition = None

        self.helis_min = 0
        self.helis_initial = 0
        self.helis_network = None
        self.heli_assets = list()

        self.drones_min = 0
        self.drones_initial = 0
        self.drones_network = None
        self.drones_assets = list()

        self.trip_targets = None

        # towed
        self.towed_number = 0
        self.towing_asset = None
        self.towed_dist = 0
        self.towed_speed = 0
        self.towed_network = None

        self.south_west = south_west
        self.north_east = north_east
        self.icao = icao
        self.airports = list()
        self.centre_position.alt_m = -10  # make sure it is out of reach
        self.magnetic_declination = magnetic_declination
        self.gci_allowed_vehicles = gci_allowed_vehicles
        self.polling_freq = polling_freq  # number of seconds before next_moving_xxx() should be called again

        self.scenario_started = 0  # time.time() when started

    @property
    def centre_lon_lat(self) -> List[float]:
        lon = self.north_east[0] - (self.north_east[0] - self.south_west[0]) / 2
        lat = self.north_east[1] - (self.north_east[1] - self.south_west[1]) / 2
        return [lon, lat]

    @property
    def centre_position(self) -> g.Position:
        lon_lat = self.centre_lon_lat
        return g.Position(lon_lat[0], lon_lat[1], 0.)

    def load_apt_dat_airports(self) -> None:
        # noinspection PyBroadException
        try:
            full_path = osp.join(u.get_scenario_resources_dir(), APT_DAT_AIRPORT_FILE)
            with open(full_path, 'rb') as file_pickle:
                all_airports = pickle.load(file_pickle)

            airports_in_boundary = list()
            for apt in all_airports:
                if self.south_west[0] <= apt.lon <= self.north_east[0] and \
                        self.south_west[1] <= apt.lat <= self.north_east[1]:
                    airports_in_boundary.append(apt)
            self.airports = airports_in_boundary
        except Exception:  # Pickle can throw pretty much any exception
            logging.exception('Could not read airports information from file')

    def add_static_targets(self, static_targets: List[StaticTarget]) -> None:
        """Add one or several static (not moving) targets to the scenario."""
        self.static_targets = static_targets

    def add_automats(self, automats: List[AutomatTarget]) -> None:
        """Add one or several automatically flying shooting fighter aircraft as a list of AutomatTargets."""
        self.automats = automats

    def add_helicopters(self, min_number: int, initial_number: int, network: nx.Graph,
                        assets: List[mpt.MPTargetAsset]) -> None:
        """Add a set of (non_shooting) helicopters, which navigate randomly from node to node in the given network.
        * min_number: how many helicopters shall there be at any time. If fewer helicopters are available,
                      then Hunter will re-spawn additional helicopters
        * initial_number: how many helicopter to start with. Helicopters are added at the beginning add one by one
                         according to parameter polling_freq in the scenario definition.
        * network: a closed non-directional graph of WayPoints.
        * assets: list of MPTargetAssets, which will be randomly assigned as actual targets
        """
        self.helis_min = min_number
        self.helis_initial = initial_number
        self.helis_network = network
        self.heli_assets = assets

    def add_drones(self, min_number: int, initial_number: int, network: nx.Graph,
                   assets: List[mpt.MPTargetAsset]) -> None:
        """Same as helicopters."""
        self.drones_min = min_number
        self.drones_initial = initial_number
        self.drones_network = network
        self.drones_assets = assets

    def add_ships(self, missile_ships: int, min_number: int, initial_number: int, network: nx.Graph,
                  assets: List[mpt.MPTargetAsset]) -> None:
        """Add a set of ships, which navigate randomly from node to node in the given network.

        Same parameters as for helicopters, apart from:
        * missile_ships: the number of missile frigates in FG instances
        """
        self.ships_missile = missile_ships
        if network is None and (missile_ships > 0 or min_number > 0):
            raise ValueError('Network for moving ships may not be None if at least 1 (missile) ship requested')
        self.ships_min = min_number
        self.ships_initial = initial_number
        self.ships_network = network
        self.ship_assets = assets

    def add_carrier(self, carrier: CarrierDefinition) -> None:
        """Add one (there can only be one aircraft carrier)."""
        self.carrier_definition = carrier

    def add_tanker(self, tanker: TankerDefinition) -> None:
        """Add one (there can only be one air to air refueler)."""
        self.tanker_definition = tanker

    def add_targets_with_trips(self, trip_targets: List[mpt.MPTargetTrips]) -> None:
        """Add targets which follow network paths in trips (random or routed based origin/destination as a list.
         Each target has its own network. There is a possibility to create convoys.
        """
        self.trip_targets = trip_targets

    def add_towed_targets(self, towed_number: int, towing_asset: Optional[mpt.MPTargetAsset], towed_dist: int,
                          towed_speed: int, towed_network: Optional[nx.DiGraph]) -> None:
        """Add one or more towed targets (a plane towing a target [e.g. delta or wind sack) in some distance.
        * towed_number: number of towed targets
        * towing_asset: list of MPTargets used to tow the towed targets (aka. tractor). Can be None if towed number is 0
        * towed_dist: the distance between the tractor and the towed targets
        * towed_speed: how fast the tractor and thereby the towed target move
        * towed_network: a closed directional graph of WayPoints. Can be None if no towed target requested
        """
        if towed_number > 0 and (towing_asset is None or towed_network is None):
            raise ValueError('Towing assets or towed network may not be empty if at least one towing is requested')
        self.towed_number = towed_number
        self.towing_asset = towing_asset
        self.towed_dist = towed_dist
        self.towed_speed = towed_speed
        self.towed_network = towed_network

    def stamp_scenario_as_started(self) -> None:
        """Sets the timestamp when the scenario was fully loaded and started.

        Used e.g. for delayed activations.
        """
        self.scenario_started = time.time()

    def reset_ships_initial(self, new_number: int) -> None:
        self.ships_initial = new_number

    def reset_helis_initial(self, new_number: int) -> None:
        self.helis_initial = new_number

    def next_moving_ship(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        yield_asset = False
        if self.ships_initial > 0:
            self.ships_initial -= 1
            yield_asset = True
        elif remaining_assets < self.ships_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.ships_network.nodes))
            asset = random.choice(self.ship_assets)
            return m.AddMovingTarget(asset, self.ships_network, starting_point)
        return None

    def next_moving_heli(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in future."""
        yield_asset = False
        if self.helis_initial > 0:
            self.helis_initial -= 1
            yield_asset = True
        elif remaining_assets < self.helis_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.helis_network.nodes))
            asset = random.choice(self.heli_assets)
            return m.AddMovingTarget(asset, self.helis_network, starting_point)
        return None

    def next_moving_drone(self, remaining_assets: int) -> Optional[m.AddMovingTarget]:
        """Yes, it is duplicated code - but expected that helis and ships will be handled differently in future."""
        yield_asset = False
        if self.drones_initial > 0:
            self.drones_initial -= 1
            yield_asset = True
        elif remaining_assets < self.drones_min:
            yield_asset = True
        if yield_asset:
            starting_point = random.choice(list(self.drones_network.nodes))
            asset = random.choice(self.drones_assets)
            return m.AddMovingTarget(asset, self.drones_network, starting_point)
        return None

    def next_trip_targets(self) -> Optional[List[mpt.MPTargetTrips]]:
        """Determine which trip targets' activation time has come, so they are run."""
        now = time.time()
        if self.trip_targets:
            activated_trip_targets = list()
            for target in reversed(self.trip_targets):
                if (now - self.scenario_started) > target.activation_delay:
                    activated_trip_targets.append(target)
                    self.trip_targets.remove(target)
            return activated_trip_targets
        return None


def load_scenario(scenario_name: str) -> ScenarioContainer:
    """Load the scenario with the provided name. If it does not exist, an AttributeError is raised.

    There must be a corresponding method called _build_scenario_NAME() in this module.
    """
    current_module = sys.modules[__name__]
    try:
        method_to_call = getattr(current_module, '_build_scenario_' + scenario_name)
        return method_to_call()
    except AttributeError:
        raise ValueError('The provided scenario name "{}" is not know.'.format(scenario_name))


def _load_network(file_name: str) -> nx.Graph:
    full_path = osp.join(u.get_scenario_resources_dir(), file_name)
    with open(full_path, 'rb') as file_pickle:
        network = pickle.load(file_pickle)
    logging.info("Loaded network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                          nx.number_of_edges(network)))
    return network


def _create_circle_points(lon_center: float, lat_center: float, radius: float, alt_m: int,
                          number_of_points: int) -> nx.DiGraph:
    """Creates a number of WayPoints clock-wise around a point starting in North."""
    graph = nx.DiGraph()
    prev_node = None
    first_node = None
    for i in range(number_of_points):
        bearing = i * 360/number_of_points  # now we look from the centre to the point
        point_lon, point_lat = g.calc_destination(lon_center, lat_center, radius, bearing)
        node = g.WayPoint(i, point_lon, point_lat, 0, alt_m)
        node.is_point_of_interest = True
        graph.add_node(node)
        if prev_node is None:
            prev_node = node
            first_node = node
        else:
            graph.add_edge(prev_node, node)
            prev_node = node
    graph.add_edge(prev_node, first_node)  # close the network
    return graph


def _construct_north_norway_ship(above_ground_m: float) -> nx.Graph:
    # Stjernøya
    wp_0 = g.WayPoint(0, 23.052063, 70.226493)
    wp_1 = g.WayPoint(1, 23., 70.325906)
    wp_2 = g.WayPoint(2, 22.841263, 70.357091)
    wp_3 = g.WayPoint(3, 22.785645, 70.402051)
    wp_4 = g.WayPoint(4, 22.532272, 70.391224)
    wp_5 = g.WayPoint(5, 22.312546, 70.345084)
    wp_6 = g.WayPoint(6, 22.397003, 70.280546)
    wp_7 = g.WayPoint(7, 22.60437, 70.242747)
    # from Stjernøya to ALTA
    wp_101 = g.WayPoint(101, 23.0809, 70.0688)
    wp_102 = g.WayPoint(102, 23.1729, 69.9793)
    wp_103 = g.WayPoint(103, 23.3116, 70.0173)

    # Seiland
    wp_8 = g.WayPoint(8, 23.118668, 70.249013)
    wp_9 = g.WayPoint(9, 23.343201, 70.291896)
    wp_10 = g.WayPoint(10, 23.681030, 70.445764)
    wp_11 = g.WayPoint(11, 23.598633, 70.586613)
    wp_12 = g.WayPoint(12, 23.584213, 70.601898)
    wp_13 = g.WayPoint(13, 23.6075559, 70.628563)
    wp_14 = g.WayPoint(14, 23.349380, 70.637216)
    wp_15 = g.WayPoint(15, 23.047256, 70.541373)
    wp_16 = g.WayPoint(16, 22.767105, 70.419778)

    # Sørøya
    wp_17 = g.WayPoint(17, 22.76878, 70.500000)
    wp_18 = g.WayPoint(18, 22.50000, 70.484337)
    wp_19 = g.WayPoint(19, 22.22122, 70.472405)

    # Silda
    wp_20 = g.WayPoint(20, 21.953430, 70.352935)
    wp_21 = g.WayPoint(21, 21.708298, 70.399057)
    wp_22 = g.WayPoint(22, 21.566162, 70.353397)
    wp_23 = g.WayPoint(23, 21.684952, 70.256437)
    wp_24 = g.WayPoint(24, 21.739197, 70.261771)

    # Northern part of Sørøya inkl. Rolvsøya
    wp_110 = g.WayPoint(110, 23.5, 70.75)
    wp_111 = g.WayPoint(111, 24., 70.75)
    wp_112 = g.WayPoint(112, 24., 70.875)
    wp_113 = g.WayPoint(113, 23.5821, 70.9113)
    wp_114 = g.WayPoint(114, 24.5, 71.)
    wp_115 = g.WayPoint(115, 24.5, 71.125)
    wp_116 = g.WayPoint(116, 23.7, 71.125)
    wp_117 = g.WayPoint(117, 23., 70.875)

    # North-Western part of Sørøya
    wp_118 = g.WayPoint(118, 22.5, 70.75)
    wp_119 = g.WayPoint(119, 22., 70.6790)
    wp_120 = g.WayPoint(120, 21.8806, 70.625)
    wp_121 = g.WayPoint(121, 22., 70.4377)
    wp_122 = g.WayPoint(122, 22.1498, 70.5761)
    wp_123 = g.WayPoint(123, 21.9616, 70.5742)
    wp_124 = g.WayPoint(124, 22.7362, 70.6608)
    wp_125 = g.WayPoint(125, 22.6263, 70.75)

    graph = nx.Graph()
    graph.add_nodes_from([wp_0, wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7,
                          wp_101, wp_102, wp_103,
                          wp_8, wp_9, wp_10, wp_11, wp_12, wp_13, wp_14, wp_15, wp_16,
                          wp_17, wp_18, wp_19,
                          wp_20, wp_21, wp_22, wp_23, wp_24,
                          wp_110, wp_111, wp_112, wp_113, wp_114, wp_115, wp_116, wp_117,
                          wp_118, wp_119, wp_120, wp_121, wp_122, wp_123, wp_124, wp_125])
    for wp in graph.nodes:
        wp.alt_m = wp.ground_m + above_ground_m
        wp.is_point_of_interest = True

    # Stjernøya
    graph.add_edge(wp_0, wp_1)
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_7, wp_0)
    # Stjernøya to Alta
    graph.add_edge(wp_0, wp_101)
    graph.add_edge(wp_101, wp_102)
    graph.add_edge(wp_102, wp_103)
    graph.add_edge(wp_103, wp_101)
    # Seiland
    graph.add_edge(wp_0, wp_8)
    graph.add_edge(wp_1, wp_8)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_9, wp_10)
    graph.add_edge(wp_10, wp_11)
    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_15, wp_16)
    graph.add_edge(wp_16, wp_3)
    graph.add_edge(wp_16, wp_4)
    # Sørøya
    graph.add_edge(wp_15, wp_17)
    graph.add_edge(wp_17, wp_18)
    graph.add_edge(wp_18, wp_19)
    graph.add_edge(wp_17, wp_16)
    graph.add_edge(wp_18, wp_4)
    graph.add_edge(wp_18, wp_5)
    graph.add_edge(wp_18, wp_16)
    graph.add_edge(wp_19, wp_5)
    # Silda
    graph.add_edge(wp_19, wp_20)
    graph.add_edge(wp_19, wp_21)
    graph.add_edge(wp_20, wp_21)
    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_23, wp_24)
    graph.add_edge(wp_24, wp_20)
    graph.add_edge(wp_20, wp_5)
    graph.add_edge(wp_20, wp_6)
    # Northern part of Sørøya inkl. Rolvsøya
    graph.add_edge(wp_13, wp_110)
    graph.add_edge(wp_14, wp_110)
    graph.add_edge(wp_110, wp_111)
    graph.add_edge(wp_111, wp_112)
    graph.add_edge(wp_112, wp_114)
    graph.add_edge(wp_114, wp_115)
    graph.add_edge(wp_115, wp_116)
    graph.add_edge(wp_116, wp_117)
    graph.add_edge(wp_110, wp_112)
    graph.add_edge(wp_110, wp_113)
    graph.add_edge(wp_112, wp_113)
    graph.add_edge(wp_113, wp_117)
    # North-Western part of Sørøya
    graph.add_edge(wp_117, wp_118)
    graph.add_edge(wp_118, wp_119)
    graph.add_edge(wp_119, wp_120)
    graph.add_edge(wp_120, wp_121)
    graph.add_edge(wp_121, wp_19)
    graph.add_edge(wp_121, wp_21)
    graph.add_edge(wp_121, wp_122)
    graph.add_edge(wp_122, wp_123)
    graph.add_edge(wp_123, wp_120)
    graph.add_edge(wp_118, wp_124)
    graph.add_edge(wp_124, wp_125)
    graph.add_edge(wp_118, wp_125)
    graph.add_edge(wp_117, wp_125)

    return graph


def _construct_north_norway_heli(above_ground_m: float) -> nx.Graph:
    # Kvaløya
    wp_1 = g.WayPoint(1, 23.8966, 70.5)
    wp_2 = g.WayPoint(2, 24., 70.5175)
    wp_3 = g.WayPoint(3, 24.1768, 70.625)
    wp_4 = g.WayPoint(4, 23.9501, 70.75)
    wp_5 = g.WayPoint(5, 23.5, 70.75)
    wp_6 = g.WayPoint(6, 23.5753, 70.625)
    wp_7 = g.WayPoint(7, 23.6329, 70.5)
    wp_8 = g.WayPoint(8, 23.6467, 70.6636)
    # Around Seiland to Alta
    wp_11 = g.WayPoint(11, 23., 70.5)
    wp_12 = g.WayPoint(12, 22.7499, 70.4124)
    wp_13 = g.WayPoint(13, 23.2045, 70.2511, 300.)
    wp_14 = g.WayPoint(14, 23.0768, 70.0328, 300.)
    wp_15 = g.WayPoint(15, 23.4091, 70., 100.)
    # Seiland to Hasvik and back
    wp_21 = g.WayPoint(21, 22.1740, 70.4802, 100.)
    wp_22 = g.WayPoint(22, 22.09917, 70.493854, 100.)
    wp_23 = g.WayPoint(23, 22., 70.4683)
    wp_24 = g.WayPoint(24, 22.1896, 70.4675, 100.)
    # Alta across the country to Hammerfest
    wp_30 = g.WayPoint(30, 23.5464, 70.0252, 1000.)
    wp_31 = g.WayPoint(31, 23.5656, 70.1151, 1000.)
    wp_32 = g.WayPoint(32, 24., 70.1888, 1000.)
    wp_33 = g.WayPoint(33, 24.5, 70.4220, 1000.)
    wp_34 = g.WayPoint(34, 24.3429, 70.4525, 1100.)
    wp_35 = g.WayPoint(35, 24.22, 70.5, 200.)
    # Across the country to/from Lakselv
    wp_41 = g.WayPoint(41, 25., 70.25, 1000.)
    wp_42 = g.WayPoint(42, 25., 70.05, 100.)
    wp_43 = g.WayPoint(43, 24.9444, 70., 1000.)
    wp_44 = g.WayPoint(44, 24.5, 70., 1500.)
    wp_45 = g.WayPoint(45, 24.85, 70.125, 1500.)

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7, wp_8,
                          wp_11, wp_12, wp_13, wp_14, wp_15,
                          wp_21, wp_22, wp_23, wp_24,
                          wp_30, wp_31, wp_32, wp_33, wp_34, wp_35,
                          wp_41, wp_42, wp_43, wp_44, wp_45])
    for wp in graph.nodes:
        wp.alt_m = wp.ground_m + above_ground_m
        wp.is_point_of_interest = True

    # Kvaløya
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_7, wp_1)
    graph.add_edge(wp_6, wp_8)
    graph.add_edge(wp_8, wp_5)
    # Around Seiland to Alta
    graph.add_edge(wp_5, wp_11)
    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_13, wp_1)
    # Seiland to Hasvik and back
    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_23, wp_24)
    graph.add_edge(wp_12, wp_21)
    # Alta across the country to Hammerfest
    graph.add_edge(wp_15, wp_30)
    graph.add_edge(wp_30, wp_31)
    graph.add_edge(wp_31, wp_32)
    graph.add_edge(wp_32, wp_33)
    graph.add_edge(wp_33, wp_34)
    graph.add_edge(wp_34, wp_35)
    graph.add_edge(wp_35, wp_2)
    # Across the country to/from Lakselv
    graph.add_edge(wp_33, wp_41)
    graph.add_edge(wp_41, wp_42)
    graph.add_edge(wp_42, wp_43)
    graph.add_edge(wp_43, wp_44)
    graph.add_edge(wp_44, wp_31)
    graph.add_edge(wp_44, wp_32)
    graph.add_edge(wp_43, wp_45)
    graph.add_edge(wp_45, wp_42)
    graph.add_edge(wp_45, wp_41)
    graph.add_edge(wp_45, wp_32)

    return graph


def _construct_north_norway_tanker(flight_level_m: float) -> nx.Graph:
    wp_1 = g.WayPoint(1, 20.5, 70.25, 0, flight_level_m)  # just north of Arnøya
    wp_2 = g.WayPoint(2, 21., 70.75, 0, flight_level_m)  # ca. 55 km to North North East
    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2])
    graph.add_edge(wp_1, wp_2)
    return graph


def _construct_sail_area_carrier_enat() -> List[Tuple[float, float]]:
    return [(24., 71.25), (23.5, 71.125), (23., 71), (22.5, 70.875), (22., 70.75), (21.5, 70.625), (21., 70.5),
            (20.5, 70.375), (20., 70.375), (20., 71.), (21., 71.25),
            (22., 71.375), (23., 71.375), (24., 71.375)]


# Callsigns need to have 2 digits at the end
OPFOR_defaults = ['OPFOR33',  # Rudolf
                  'OPFOR34',   # Kokos
                  'OPFOR42',  # Leto
                  'OPFOR69',  # vanosten
                  'OPFOR77',  # Pinto
                  'OPFOR78'  # swamp
                  ]


# Assets are randomly chosen. If they appear more often, then the probability gets higher
default_ships_list = [mpt.oprf_frigate, mpt.us_navy_lake_champlain, mpt.us_navy_normandy, mpt.us_navy_oliver_perry,
                      mpt.us_navy_san_antonio]
default_helis_list = [mpt.blue_tigre, mpt.blue_ch_47, mpt.red_mi_8, mpt.red_mi_24]
default_drones_list = [mpt.blue_mq_9]

default_rails_list = [mpt.MPTarget.TRUCK, mpt.MPTarget.BUK_M2]


def _build_scenario_north_norway() -> ScenarioContainer:
    static_targets = [
        # Airport ENAT Alta
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.3584, 69.9709, g.feet_to_metres(187.)), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(23.2561, 69.8920, 22), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.2868, 69.9833, g.feet_to_metres(610.)), 0, 1),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(23.3429, 69.9805, 0), 120),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(23.3466, 69.9798, 0), 120),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(23.3555, 69.9775, 3), 30),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(23.3830, 69.9722, 1), 217),
        StaticTarget(mpt.MPTarget.BRIDGE, g.Position(23.3750, 69.9685, 0), 152),
        StaticTarget(mpt.MPTarget.POWER_PLANT, g.Position(23.5154, 70.0239, 7), 0),
        # Airport ENHF Hammerfest
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(23.6673, 70.6805, 81), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(23.6752, 70.6820, 80), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(23.6513, 70.6789, 76), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(23.6793, 70.6803, 77), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.6665, 70.6796, 79), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.6776, 70.6827, 80), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.6745, 70.6855, 159), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.7201, 70.6806, 191), 0, 2),
        # Airport ENHV Honningsvåg
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(25.9764, 71.0091, 3), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9703, 71.0093, 0), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9963, 71.0102, 3), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9786, 71.0091, 5), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(25.9803, 70.9878, 278), 0, 3),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(25.8925, 70.9844, 377), 0, 3),
        # airport ENNA Lakselv
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9792, 70.0577, 7), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9699, 70.064, 9), 85),  # along taxiway from south to north
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9688, 70.0686, 8), 85),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(24.9678, 70.0715, 5), 0),
        StaticTarget(mpt.MPTarget.HARD_SHELTER, g.Position(24.9676, 70.0737, 3.5), 270),  # north end
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9730, 70.0555, 13), 45),  # south end
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(24.9847, 70.0628, 2), 110),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(24.9828, 70.0531, 8), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9689, 70.0801, 1), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(24.9946, 70.0493, 12), 0, 1),
        # StaticTarget(wa.MPTarget.GASOMETER, g.Position(24.9818, 70.0669, 3), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(24.9833, 70.0648, 3), 0),
        StaticTarget(mpt.MPTarget.OILRIG, g.Position(25.3508, 70.3837, 0), 0),
        # airport ENHK Hasvik
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(22.1347, 70.4861, 5), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(22.1421, 70.4848, 10), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(22.2054, 70.4917, 46), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(22.1351, 70.4966, 1), 0, 2),
        # StaticTarget(MPTarget.TRUCK, g.Position(22.1384, 70.4865, 10), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(22.1448, 70.4845, 12), 0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(22.1558, 70.4837, 0), 0),
        # North-East to ENAT along the street
        # StaticTarget(MPTarget.TRUCK, g.Position(23.8913, 70.1761, 383), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.9093, 70.1769, 352), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.9321, 70.1794, 345), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.9693, 70.1849, 326), 0, 3),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.5114, 70.4236, 85), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.5110, 70.4268, 77), 0, 3),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(24.5077, 70.4177, 107), 0),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.9109, 70.2044, 18), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9091, 70.2133, 41), 0, 1),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.2908, 69.9208, 475), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.3015, 69.9244, 469), 0, 2),
        # StaticTarget(MPTarget.TRUCK, g.Position(24.2836, 69.9192, 468), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(24.2759, 69.9173, 468), 0),
    ]
    automat_1 = AutomatTarget('br-enhf.fgfp', AutomatType.mig_29, True, True,
                              True, True, 150,
                              4000, 8000, 0, False, 50,
                              True, 1, 1, 3, tacview=False)
    automat_2 = AutomatTarget('br-enna.fgfp', AutomatType.su_27, True, True,
                              True, True, 150,
                              4000, 8000, 0, False, 50,
                              True, 1, 2, 3, tacview=False)
    vinson_carrier = CarrierDefinition(fgt.CarrierType.vinson, _construct_sail_area_carrier_enat(), (21.5, 70.9))
    tanker = TankerDefinition(ENAT, _construct_north_norway_tanker(g.feet_to_metres(20000)))
    scenario = ScenarioContainer('north_norway', 'North Norway',  'Cf. http://opredflag.com/forum_threads/3154248',
                                 OPFOR_defaults,
                                 (20., 69.5), (27., 71.5),
                                 'ENNA', 12.3, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_automats([automat_1, automat_2])
    scenario.add_carrier(vinson_carrier)
    scenario.add_tanker(tanker)
    scenario.add_helicopters(3, 5, _construct_north_norway_heli(100.), default_helis_list)
    scenario.add_drones(3, 5, _construct_north_norway_heli(100.), default_drones_list)
    scenario.add_ships(3, 5, 8, _construct_north_norway_ship(0.), default_ships_list)
    return scenario


def _construct_dammastock_circle() -> nx.DiGraph:
    return _create_circle_points(8.42, 46.643, 10000, int(g.feet_to_metres(12000)), 18)


def _construct_alpnach_see_speedboat_track() -> nx.Graph:
    wp_1 = g.WayPoint(1, 8.28858, 46.9573, alt_m=433.995)
    wp_2 = g.WayPoint(2, 8.28507, 46.9558, alt_m=433.998)
    wp_3 = g.WayPoint(3, 8.28111, 46.9566, alt_m=433.997)
    wp_4 = g.WayPoint(4, 8.28604, 46.9598, alt_m=433.997)
    wp_5 = g.WayPoint(5, 8.28903, 46.9602, alt_m=433.994)
    wp_6 = g.WayPoint(6, 8.29531, 46.9631, alt_m=433.988)
    wp_7 = g.WayPoint(7, 8.31209, 46.968, alt_m=433.996)
    wp_8 = g.WayPoint(8, 8.29588, 46.9534, alt_m=433.988)
    wp_9 = g.WayPoint(9, 8.31757, 46.9624, alt_m=433.99)
    wp_10 = g.WayPoint(10, 8.32711, 46.9673, alt_m=433.988)
    wp_11 = g.WayPoint(11, 8.33434, 46.9718, alt_m=433.995)
    wp_12 = g.WayPoint(12, 8.33426, 46.975, alt_m=433.997)
    wp_13 = g.WayPoint(13, 8.33218, 46.976, alt_m=433.999)
    wp_14 = g.WayPoint(14, 8.33128, 46.9729, alt_m=433.997)
    wp_15 = g.WayPoint(15, 8.32346, 46.9693, alt_m=433.988)

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7, wp_8, wp_9,
                          wp_10, wp_11, wp_12, wp_13, wp_14, wp_15])

    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_7, wp_15)
    graph.add_edge(wp_1, wp_6)
    graph.add_edge(wp_1, wp_8)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_9, wp_15)
    graph.add_edge(wp_9, wp_10)
    graph.add_edge(wp_10, wp_11)
    graph.add_edge(wp_10, wp_15)
    graph.add_edge(wp_10, wp_14)
    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)

    return graph


def _construct_lsmi_truck_track() -> nx.Graph:
    wp_1 = g.WayPoint(1, 7.88845, 46.6828, alt_m=572.326)
    wp_2 = g.WayPoint(2, 7.88614, 46.6843, alt_m=569.999)
    wp_3 = g.WayPoint(3, 7.8842, 46.6848, alt_m=569.318)
    wp_4 = g.WayPoint(4, 7.88334, 46.6835, alt_m=570.203)
    wp_5 = g.WayPoint(5, 7.88326, 46.6832, alt_m=570.407)
    wp_6 = g.WayPoint(6, 7.88692, 46.6818, alt_m=572.526)
    wp_7 = g.WayPoint(7, 7.88103, 46.6827, alt_m=570.745)
    wp_8 = g.WayPoint(8, 7.88007, 46.6827, alt_m=570.853)
    wp_9 = g.WayPoint(9, 7.8793, 46.6835, alt_m=570.49)
    wp_10 = g.WayPoint(10, 7.87698, 46.6834, alt_m=571.04)
    # North East corner taken out due to buildings in OSM
    # wp_11 = g.WayPoint(11, 7.87729, 46.6842, alt_m=570.46)
    # wp_12 = g.WayPoint(12, 7.87822, 46.6847, alt_m=569.882)
    # wp_13 = g.WayPoint(13, 7.87955, 46.6851, alt_m=569.374)
    wp_14 = g.WayPoint(14, 7.87752, 46.682, alt_m=571.763)
    wp_15 = g.WayPoint(15, 7.87841, 46.6811, alt_m=572.15)
    wp_16 = g.WayPoint(16, 7.8768, 46.6765, alt_m=575.292)
    wp_17 = g.WayPoint(17, 7.87794, 46.6758, alt_m=575.804)
    wp_18 = g.WayPoint(18, 7.86908, 46.6712, alt_m=579.742)
    wp_19 = g.WayPoint(19, 7.8698, 46.6703, alt_m=580.24)
    wp_20 = g.WayPoint(20, 7.87131, 46.6697, alt_m=580.715)
    wp_21 = g.WayPoint(21, 7.87328, 46.6702, alt_m=580.383)
    wp_22 = g.WayPoint(22, 7.881, 46.6761, alt_m=576.162)
    wp_23 = g.WayPoint(23, 7.87908, 46.6766, alt_m=575.25)
    wp_24 = g.WayPoint(24, 7.88472, 46.6798, alt_m=573.606)
    wp_25 = g.WayPoint(25, 7.88529, 46.6807, alt_m=572.983)

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7, wp_8, wp_9,
                          wp_10, wp_14, wp_15, wp_16, wp_17, wp_18, wp_19,  # , wp_11, wp_12, wp_13
                          wp_20, wp_21, wp_22, wp_23, wp_24, wp_25])

    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_1)
    graph.add_edge(wp_5, wp_7)
    graph.add_edge(wp_7, wp_8)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_9, wp_4)
    graph.add_edge(wp_9, wp_10)
    # graph.add_edge(wp_10, wp_11)
    # graph.add_edge(wp_11, wp_12)
    # graph.add_edge(wp_12, wp_13)
    # graph.add_edge(wp_13, wp_3)
    graph.add_edge(wp_10, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_15, wp_16)
    graph.add_edge(wp_16, wp_17)
    graph.add_edge(wp_16, wp_18)
    graph.add_edge(wp_18, wp_19)
    graph.add_edge(wp_19, wp_20)
    graph.add_edge(wp_20, wp_21)
    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_22, wp_24)
    graph.add_edge(wp_24, wp_25)
    graph.add_edge(wp_25, wp_6)
    graph.add_edge(wp_25, wp_23)
    graph.add_edge(wp_23, wp_17)
    graph.add_edge(wp_17, wp_19)

    return graph


def _build_scenario_swiss() -> ScenarioContainer:
    static_targets = [
        # Axalp
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(8.0547, 46.7057, 2180), 70),  # Grätli left
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(8.0551, 46.7051, 2165), 60),  # Grätli right
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(8.05934685, 46.70961520, 2237), 0),  # Axalphorn
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(8.0617, 46.7011, 2230), 165),  # Felswand
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(6.88, 46.88, 419), 130)  # Forel
    ]
    # A truck driving on the runway and taxiway as LSMI
    lsmi_graph = _construct_lsmi_truck_track()
    origin_lsmi = g.Position(7.88845, 46.6828)  # same as WP_1
    lsmi_truck = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, lsmi_graph, origin_lsmi,
                                                        mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)
    # A random trip speedboat on Alpnach Lake
    lsma_graph = _construct_alpnach_see_speedboat_track()
    origin_lsma = g.Position(8.28858, 46.9573)
    lsma_speedboat = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.SPEEDBOAT, lsma_graph, origin_lsma,
                                                            mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)
    # a convoy of trucks close to LSMP
    # network created with road_network.py: TEST/params.py -b *6.75_46.75_7_46.875 -o swiss_lsmp_roads.pkl
    lsmp_trip_graph = _load_network('swiss_lsmp_roads.pkl')
    origin_trip_lsmp = g.Position(6.7813, 46.8112)  # Cheyres
    dests_lsmp = [g.Position(6.9595, 46.8387),  # Corcelles
                  g.Position(6.8319, 46.7803),  # Nuvilly
                  g.Position(6.9280, 46.8721),  # Grandcour
                  g.Position(6.9387, 46.7542)  # Chatonnaye
                  ]
    lsmp_convoy = mpt.MPTargetTrips.create_routed_convoy_targets(mpt.MPTarget.TRUCK,
                                                                 lsmp_trip_graph, origin_trip_lsmp,
                                                                 mpt.MPTargetTrips.STATIC_HP, 5, dests_lsmp, True,
                                                                 30, 3, 2)

    # a convoy of trucks running around lake Murten
    # network created with road_network.py: -f TEST/params.py -b *7.018_46.9_7.148_46.97 -o swiss_murten_roads.pkl
    murten_trip_graph = _load_network('swiss_murten_roads.pkl')
    origin_trip_murten = g.Position(7.0253, 46.9200)  # Salavaux
    dests_murten = [g.Position(7.0752, 46.9083),  # Faoug
                    g.Position(7.1339, 46.9396),  # Muntelier
                    g.Position(7.1139, 46.9628),  # Sugiez
                    g.Position(7.0253, 46.9200)  # Salavaux
                    ]
    murten_convoy = mpt.MPTargetTrips.create_routed_convoy_targets(mpt.MPTarget.TRUCK,
                                                                   murten_trip_graph, origin_trip_murten,
                                                                   mpt.MPTargetTrips.STATIC_HP, 0, dests_murten, True,
                                                                   40, 5, 2)

    trip_targets = [lsmi_truck, lsma_speedboat]
    trip_targets.extend(lsmp_convoy)
    trip_targets.extend(murten_convoy)

    # network created with heli_network.py: -f TEST/params.py -b *8_46.75_8.75_47.125 -o swiss_helis.pkl
    heli_network = _load_network('swiss_helis.pkl')

    scenario = ScenarioContainer('swiss', 'Swiss', 'Swiss Air Force shooting range Axalp and other training targets',
                                 OPFOR_defaults,
                                 (6.7, 46.5), (8.8, 47.1),
                                 'LSMM', 2.5, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_targets_with_trips(trip_targets)
    scenario.add_towed_targets(2, mpt.pc9, 500, 120, _construct_dammastock_circle())
    scenario.add_helicopters(3, 5, heli_network, default_helis_list)
    return scenario


def _construct_croatia_grgurov_kanal_speedboat_track() -> nx.Graph:
    # island of Prvić
    wp_1 = g.WayPoint(1, 14.766, 44.9358, alt_m=0)
    wp_2 = g.WayPoint(2, 14.7934, 44.9315, alt_m=0)
    wp_3 = g.WayPoint(3, 14.8354, 44.9045, alt_m=0)
    wp_4 = g.WayPoint(4, 14.8412, 44.8814, alt_m=0)
    wp_5 = g.WayPoint(5, 14.7643, 44.9109, alt_m=0)
    # island of Sveti Grgur
    wp_11 = g.WayPoint(11, 14.7457, 44.8878, alt_m=0)
    wp_12 = g.WayPoint(12, 14.7895, 44.8618, alt_m=0)
    wp_13 = g.WayPoint(13, 14.7718, 44.8519, alt_m=0)
    wp_14 = g.WayPoint(14, 14.7391, 44.8609, alt_m=0)
    wp_15 = g.WayPoint(15, 14.7317, 44.8769, alt_m=0)
    wp_16 = g.WayPoint(16, 14.7424, 44.8773, alt_m=0)
    # island of Goli otok
    wp_21 = g.WayPoint(21, 14.7963, 44.8536, alt_m=0)
    wp_22 = g.WayPoint(22, 14.8426, 44.8495, alt_m=0)
    wp_23 = g.WayPoint(23, 14.8400, 44.8356, alt_m=0)
    wp_24 = g.WayPoint(24, 14.8293, 44.8305, alt_m=0)
    wp_25 = g.WayPoint(25, 14.8300, 44.8204, alt_m=0)
    wp_26 = g.WayPoint(26, 14.7993, 44.8411, alt_m=0)
    # island of Rab
    wp_41 = g.WayPoint(41, 14.7678, 44.8472, alt_m=0)
    wp_42 = g.WayPoint(42, 14.7574, 44.8218, alt_m=0)
    wp_43 = g.WayPoint(43, 14.7497, 44.8150, alt_m=0)
    wp_44 = g.WayPoint(44, 14.8295, 44.7590, alt_m=0)
    wp_45 = g.WayPoint(45, 14.8784, 44.7215, alt_m=0)
    wp_46 = g.WayPoint(46, 14.8514, 44.6901, alt_m=0)
    wp_47 = g.WayPoint(47, 14.7951, 44.7336, alt_m=0)
    wp_48 = g.WayPoint(48, 14.7647, 44.7467, alt_m=0)
    wp_49 = g.WayPoint(49, 14.7594, 44.7436, alt_m=0)
    wp_50 = g.WayPoint(50, 14.6889, 44.7560, alt_m=0)
    wp_51 = g.WayPoint(51, 14.6523, 44.7918, alt_m=0)
    wp_52 = g.WayPoint(52, 14.6935, 44.8232, alt_m=0)
    wp_53 = g.WayPoint(53, 14.6716, 44.8467, alt_m=0)
    wp_54 = g.WayPoint(54, 14.7373, 44.8574, alt_m=0)

    # Coast
    wp_61 = g.WayPoint(61, 14.8720, 44.8162, alt_m=0)  # Kladd
    wp_62 = g.WayPoint(62, 14.8761, 44.7925, alt_m=0)  # Starigrad
    wp_63 = g.WayPoint(63, 14.8792, 44.7129, alt_m=0)  # Stinica

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5,
                          wp_11, wp_12, wp_13, wp_14, wp_15, wp_16,
                          wp_21, wp_22, wp_23, wp_24, wp_25, wp_26,
                          wp_41, wp_42, wp_43, wp_44, wp_45, wp_46, wp_47, wp_48, wp_49,
                          wp_50, wp_51, wp_52, wp_53, wp_54,
                          wp_61, wp_62, wp_63])

    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_1)

    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_15, wp_16)
    graph.add_edge(wp_16, wp_11)

    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_23, wp_24)
    graph.add_edge(wp_24, wp_25)
    graph.add_edge(wp_25, wp_26)
    graph.add_edge(wp_26, wp_21)

    graph.add_edge(wp_5, wp_11)
    graph.add_edge(wp_4, wp_21)
    graph.add_edge(wp_12, wp_21)
    graph.add_edge(wp_13, wp_21)
    graph.add_edge(wp_13, wp_26)

    # Rab
    graph.add_edge(wp_41, wp_42)
    graph.add_edge(wp_42, wp_43)
    graph.add_edge(wp_43, wp_44)
    graph.add_edge(wp_44, wp_45)
    # not 45-46
    graph.add_edge(wp_45, wp_63)
    graph.add_edge(wp_63, wp_46)
    graph.add_edge(wp_46, wp_47)
    graph.add_edge(wp_47, wp_48)
    graph.add_edge(wp_48, wp_49)
    graph.add_edge(wp_49, wp_50)
    graph.add_edge(wp_50, wp_51)
    graph.add_edge(wp_51, wp_52)
    graph.add_edge(wp_52, wp_53)
    graph.add_edge(wp_53, wp_54)
    graph.add_edge(wp_54, wp_41)

    # coast and connections
    graph.add_edge(wp_61, wp_62)
    graph.add_edge(wp_62, wp_63)
    graph.add_edge(wp_45, wp_62)
    graph.add_edge(wp_23, wp_61)
    graph.add_edge(wp_25, wp_61)
    graph.add_edge(wp_14, wp_54)
    graph.add_edge(wp_13, wp_41)
    graph.add_edge(wp_26, wp_41)

    return graph


def _construct_sail_area_carrier_adria() -> List[Tuple[float, float]]:
    return [(13.66, 44.83),  # Pula
            (12.5, 44.5),  # Ravenna
            (13.6, 43.67),  # Ancona
            (14.35, 42.55),  # Pescara
            (16.0, 43.28)  # Split
            ]


def _construct_croatia_tanker(flight_level_m: float) -> nx.Graph:
    wp_1 = g.WayPoint(1, 13.43, 44.1, 0, flight_level_m)
    wp_2 = g.WayPoint(2, 14.17, 44.1, 0, flight_level_m)
    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2])
    graph.add_edge(wp_1, wp_2)
    return graph


def _build_scenario_croatia() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.45134603, 45.12084095, 315.22), 190),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.47817711, 45.15597383, 278.89), 225),
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(15.46642110, 45.15227261, 258.38), 0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.35403661, 45.17658397, 415.64), 350),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.35605100, 45.17734675, 407.75), 350),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.35764889, 45.17402033, 461.74), 350),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.44395520, 45.13041282, 285.39), 105),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.44569273, 45.13307877, 281.03), 105),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.43930508, 45.13526179, 285.17), 105),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.43756785, 45.13230291, 295.75), 105),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.47054590, 45.14772888, 259.03), 121),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48843116, 45.13102892, 279.85), 330),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48885112, 45.13049413, 280.77), 330),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48919529, 45.13005391, 281.47), 330),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48958546, 45.12957522, 280.956), 330),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(15.48677027, 45.08705502, 374.29), 24),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.49388525, 45.09844544, 279.30), 60),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.49090056, 45.09978517, 284.96), 60),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48753297, 45.10134146, 293.71), 60),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48461009, 45.10268827, 289.39), 60),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.49277869, 45.11349851, 282.90), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48581480, 45.11163144, 286.13), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48928243, 45.11853318, 282.78), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48252808, 45.11650189, 293.97), 70),
        StaticTarget(mpt.MPTarget.S_300, g.Position(15.54694001, 45.05560442, 389.95), 32),
        StaticTarget(mpt.MPTarget.S_300, g.Position(15.48705412, 45.15422394, 354.75), 96),
        StaticTarget(mpt.MPTarget.S_300, g.Position(15.42062364, 45.18303639, 298.64), 4),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.46957855, 45.11545707, 364.92), 310),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(15.44195858, 45.17045487, 255.98), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(15.46779326, 45.13225982, 300.97), 265),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.50651187, 45.10753776, 275.17), 0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.50637840, 45.10665494, 277.76), 0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.50528944, 45.10668555, 280.64), 0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.50540209, 45.10758750, 278.06), 0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.52031985, 45.09005467, 274.88), 336),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.52174861, 45.08477434, 288.44), 336),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.56593041, 45.05902512, 427.084), 30),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.56397648, 45.06171625, 444.30), 30),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(15.46176065, 45.10051699, 495.03), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.51104405, 45.11098530, 263.94), 240),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.52441082, 45.06357041, 528.66), 48),
        # lake / sump at South-West corner just a bit outside
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4052, 45.0218, 484), 80),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4058, 45.0181, 484), 260),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4136, 45.0162, 485), 180),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(15.4246, 45.0212, 607), 60),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(15.4239, 45.0224, 617), 60)
    ]
    speedboat_graph = _construct_croatia_grgurov_kanal_speedboat_track()
    origin_1 = g.Position(14.766, 44.9358)  # WP_1
    speedboat_1 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.SPEEDBOAT, speedboat_graph, origin_1,
                                                         mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)
    origin_2 = g.Position(14.8300, 44.8204)  # WP_25
    speedboat_2 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.SPEEDBOAT, speedboat_graph, origin_2,
                                                         mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)

    # a convoy of trucks close to Slunj at Glavicica
    # road_network.py: -f TEST/params.py -b *15.526_45.106_15.545_45.122 -o croatia_glavicica_roads.pkl -s
    convoy_graph = _load_network('croatia_glavicica_roads.pkl')
    origin_trip_convoy = g.Position(15.5429, 45.1121)  # helicopter North-West
    dests_convoy = [g.Position(15.5320, 45.1098),
                    g.Position(15.5281, 45.1125),
                    g.Position(15.5335, 45.1171)  # Glavicica
                    ]
    convoy = mpt.MPTargetTrips.create_routed_convoy_targets(mpt.MPTarget.TRUCK,
                                                            convoy_graph, origin_trip_convoy,
                                                            mpt.MPTargetTrips.STATIC_HP, 5, dests_convoy, True,
                                                            30, 3, 2)

    trip_targets = [speedboat_1, speedboat_2]
    trip_targets.extend(convoy)

    vinson_carrier = CarrierDefinition(fgt.CarrierType.vinson, _construct_sail_area_carrier_adria(), (14.25, 44.0))

    tanker = TankerDefinition(LIPA, _construct_croatia_tanker(g.feet_to_metres(20000)))

    scenario = ScenarioContainer('croatia', 'Eugen Kvaternik', 'Croatian Military Range near Slunj',
                                 OPFOR_defaults,
                                 (14.5, 44.625), (15.75, 45.25),
                                 'LDRI', 4.0, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_targets_with_trips(trip_targets)
    scenario.add_carrier(vinson_carrier)
    scenario.add_tanker(tanker)
    return scenario


def _construct_falklands_ship(above_ground_m: float) -> nx.Graph:
    wp_1 = g.WayPoint(1, -59.3261, -51.1776)
    wp_2 = g.WayPoint(2, -59.3014, -51.2653)
    wp_3 = g.WayPoint(3, -59.5047, -51.1362)
    wp_4 = g.WayPoint(4, -60.6939, -51.1845)
    wp_5 = g.WayPoint(5, -60.9356, -51.5360)
    wp_6 = g.WayPoint(6, -61.3064, -51.5224)
    wp_7 = g.WayPoint(7, -60.7873, -51.7542)
    wp_8 = g.WayPoint(8, -61.5151, -51.8459)
    wp_9 = g.WayPoint(9, -60.6582, -52.3118)
    wp_10 = g.WayPoint(10, -60.0512, -52.2177)
    wp_11 = g.WayPoint(11, -60.3945, -52.3839)

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7, wp_8, wp_9, wp_10, wp_11])
    for wp in graph.nodes:
        wp.alt_m = wp.ground_m + above_ground_m
        wp.is_point_of_interest = True

    # edges
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_1, wp_3)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_4, wp_6)
    graph.add_edge(wp_5, wp_7)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_6, wp_8)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_9, wp_10)
    graph.add_edge(wp_9, wp_11)
    graph.add_edge(wp_10, wp_11)

    return graph


def _build_scenario_falklands() -> ScenarioContainer:
    static_targets = [
        # Port Howard SFPH
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-59.50651585, -51.62629475, 1), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-59.52009521, -51.63214343, 1), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-59.51432636, -51.61269427, 17), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.52070372, -51.61498746, 7), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.52368429, -51.61193519, 18), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.50628052, -51.61192936, 1), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-59.50338468, -51.62123505, 1), 0, 3),

        # Chartres SFCS
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.06014292, -51.71829692, 10), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.06904195, -51.71512290, 6), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.05780592, -51.72231465, 7), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.05508825, -51.72659592, 2), 0, 1),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.06391101, -51.73510859, 81), 0),

        # Dunnose Head SFDH
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.41455806, -51.75474579, 9), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.41779578, -51.75019133, 19), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.40498462, -51.75773028, 1), 0, 2),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.41800430, -51.76342062, 5), 0),

        # Shallow Harbour SFSW
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.52336272, -51.76004950, 16), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.52216032, -51.75124282, 40), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.51654578, -51.74090384, 111), 0, 2),

        # Spring Point SFSP
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.45442221, -51.83032886, 19), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.44393529, -51.83290163, 17), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.46448109, -51.83920252, 1), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.48275161, -51.83544599, 10), 0, 1),

        # Port Stephens SFPS
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.83463924, -52.07935587, 18), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.85245786, -52.09274383, 17), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.84476475, -52.10766650, 1), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.84253921, -52.08965744, 22), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.84506353, -52.12238839, 1), 0),

        # West Point SFWP
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.66546029, -51.35547304, 3), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.64381050, -51.34636112, 12), 0, 3),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.69391652, -51.34264992, 12), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.68366466, -51.34916960, 10), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.63344255, -51.37084483, 16), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.69487360, -51.37863192, 2), 0),

        # Crodendle House SFCH
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.26388741, -51.52671908, 38), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.25399085, -51.53174845, 30), 0, 3),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.25744850, -51.54523981, 46), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.25824423, -51.51302880, 56), 0),

        # Hill Cove SFHC
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.14780072, -51.50589489, 17), 0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.13912595, -51.50633961, 18), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.15864713, -51.48810510, 1), 0, 3),

        # North Arm SFNA
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-59.37846855, -52.12489388, 14), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.36832970, -52.12551483, 8), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-59.37492797, -52.14119141, 7), 1),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.35872516, -52.13147986, 7), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.38594129, -52.13293278, 12), 0),

        # Fox Bay SFFB
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.06258578, -51.93542851, 29), 0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.05899565, -51.95788521, 9), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.06694972, -51.92370182, 26), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.07676724, -51.93897830, 6), 1),
    ]
    scenario = ScenarioContainer('falklands', 'Falklands',
                                 'Cf. http://opredflag.com/events/923128?event_instance_id=15944889',
                                 OPFOR_defaults,
                                 (-61., -52.5), (-57.5, -51.2),
                                 'EGYP', 2.0, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_ships(2, 0, 3, _construct_falklands_ship(0.), [mpt.oprf_frigate])
    return scenario


def _construct_northern_mariana_ship(above_ground_m: float) -> nx.Graph:
    wp_1 = g.WayPoint(1, 145.541739, 15.029769)
    wp_2 = g.WayPoint(2, 145.547782, 15.091035)
    wp_3 = g.WayPoint(3, 145.615004, 15.139890)
    wp_4 = g.WayPoint(4, 145.631621, 15.217889)
    wp_5 = g.WayPoint(5, 145.719237, 15.234651)
    wp_6 = g.WayPoint(6, 145.778906, 15.308972)
    wp_7 = g.WayPoint(7, 145.865012, 15.297316)
    wp_8 = g.WayPoint(8, 145.883139, 15.263800)
    wp_9 = g.WayPoint(9, 145.814406, 15.203312)
    wp_10 = g.WayPoint(10, 145.824980, 15.130412)
    wp_11 = g.WayPoint(11, 145.787970, 15.098328)
    wp_12 = g.WayPoint(12, 145.815161, 15.041440)
    wp_13 = g.WayPoint(13, 145.892958, 15.048005)
    wp_14 = g.WayPoint(14, 145.894469, 14.971403)
    wp_15 = g.WayPoint(15, 145.800055, 14.964836)
    # wp_0 = g.WayPoint(, 145.813651, 15.043628)
    wp_17 = g.WayPoint(17, 145.756247, 15.057487)
    wp_18 = g.WayPoint(18, 145.707907, 15.098328)
    # wp_0 = g.WayPoint(0, 145.615759, 15.137703)
    wp_20 = g.WayPoint(20, 145.681471, 15.093223)
    wp_21 = g.WayPoint(21, 145.712439, 15.046546)
    wp_22 = g.WayPoint(22, 145.723769, 14.973592)
    wp_23 = g.WayPoint(23, 145.688269, 14.914481)
    wp_24 = g.WayPoint(24, 145.620291, 14.894044)
    wp_25 = g.WayPoint(25, 145.593855, 14.923969)
    wp_26 = g.WayPoint(26, 145.522856, 14.886014)
    wp_27 = g.WayPoint(27, 145.489623, 14.953890)
    wp_28 = g.WayPoint(28, 145.540984, 14.979429)
    # wp_0 = g.WayPoint(0, 145.593100, 14.925429)
    # wp_0 = g.WayPoint(0, 145.542494, 15.030498)

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6, wp_7, wp_8, wp_9, wp_10,
                          wp_11, wp_12, wp_13, wp_14, wp_15, wp_17, wp_18,
                          wp_20, wp_21, wp_22, wp_23, wp_24, wp_26, wp_27, wp_28])
    for wp in graph.nodes:
        wp.alt_m = wp.ground_m + above_ground_m
        wp.is_point_of_interest = True

    # edges
    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_6)
    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_7, wp_8)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_9, wp_10)
    graph.add_edge(wp_10, wp_11)
    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_15, wp_12)
    graph.add_edge(wp_12, wp_17)
    graph.add_edge(wp_17, wp_18)
    graph.add_edge(wp_18, wp_3)
    graph.add_edge(wp_3, wp_20)
    graph.add_edge(wp_20, wp_21)
    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_23, wp_24)
    graph.add_edge(wp_24, wp_25)
    graph.add_edge(wp_25, wp_26)
    graph.add_edge(wp_26, wp_27)
    graph.add_edge(wp_27, wp_28)
    graph.add_edge(wp_28, wp_25)
    graph.add_edge(wp_25, wp_1)
    graph.add_edge(wp_27, wp_1)
    graph.add_edge(wp_28, wp_1)
    graph.add_edge(wp_15, wp_22)
    graph.add_edge(wp_22, wp_17)
    graph.add_edge(wp_17, wp_11)

    return graph


def _build_scenario_northern_mariana() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(145.66579653, 14.95363634, 2.8763), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(145.65159095, 14.95604869, 182.8701), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(145.55179930, 14.84919640, 157.4966), 0, 2),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(145.62759909, 15.04078500, 154.6720), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(145.63128228, 15.01646323, 83.5844), 240.0, 1),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(145.72406157, 15.12155346, 65.5575), 85.0),
        # StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72547640, 15.12485989, 63.1279), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.7267, 15.1258, 63.1279), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72545999, 15.12391426, 63.6506), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72694381, 15.12315665, 65.5359), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72666452, 15.12467371, 63.6134), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.72881483, 15.12316435, 66.2967), 204.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.72949293, 15.12345911, 66.3027), 204.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.73020012, 15.12377071, 66.3147), 204.0),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(145.72379214, 15.12331304, 62.7219), 292.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(145.72582705, 15.12042629, 67.4474), 148.0)
    ]
    scenario = ScenarioContainer('northern_mariana', 'Northern Mariana',
                                 'http://opredflag.com/events/929815?event_instance_id=16025063',
                                 OPFOR_defaults,
                                 (143.75, 14.76), (146, 15.375),
                                 'PGSN', 0.3, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_ships(3, 0, 0, _construct_northern_mariana_ship(0.), default_ships_list)
    return scenario


def _build_scenario_frisian_flag() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(4.94186, 51.4449, 27.466), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(4.93258, 51.4347, 28.8972), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(4.91833, 51.4459, 23.5435), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(4.93008, 51.4515, 27.0605), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(4.90277, 51.503, 18.2422), 0, 2),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(4.99225, 51.4443, 25.3366), 0, 2),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(4.85623, 51.4302, 16.2653), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(4.94867, 51.5751, 8.96947), 0, 1)
    ]
    automat_1 = AutomatTarget('frisflag-2.fgfp', AutomatType.f_16, True, True,
                              True, True, 150,
                              0, 2000, 0, False, 30,
                              True, 1, 1, 1, tacview=False)
    automat_2 = AutomatTarget('frisflag-2.fgfp', AutomatType.f_16, True, True,
                              True, True, 150,
                              0, 2000, 0, False, 50,
                              True, 1, 1, 1, tacview=False)
    scenario = ScenarioContainer('frisian_flag', 'Frisian Flag 2020',
                                 'http://opredflag.com/events/933083?event_instance_id=16107887',
                                 OPFOR_defaults,
                                 (4.8, 51.4), (5.0, 51.6),
                                 'EHOW', 2.0, None, 60)
    scenario.add_static_targets(static_targets)
    scenario.add_automats([automat_1, automat_2])
    return scenario


def _build_scenario_eastern_edge() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(44.57241322, 40.17616257, 1343.4061), 312.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(44.44050714, 40.12849988, 888.3779), 312.0, 1),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(44.39579910, 40.14533647, 855.0697), 91.2),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(44.56710058, 40.29689654, 1351.3494), 216.0),

        StaticTarget(mpt.MPTarget.DEPOT, g.Position(44.52005910, 40.80191240, 1415.3240), 168.0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(44.49295865, 40.81617438, 1307.8225), 76.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(44.51726290, 40.79210229, 1387.1468), 0.0),
        StaticTarget(mpt.MPTarget.BRIDGE, g.Position(44.49507787, 40.81573426, 1310.3151), 1.6),
        StaticTarget(mpt.MPTarget.HQ, g.Position(44.44110944, 40.83755432, 1387.2861), 268.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(44.57084901, 40.78251061, 1702.7911), 0.0, 1),

        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(45.46095196, 40.17780984, 1917.0751), 0.0, 1),
        StaticTarget(mpt.MPTarget.S_300, g.Position(45.18958695, 40.27971637, 2021.5598), 0.0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(45.75087972, 40.17960826, 1977.7098), 0.0, 1),

        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(44.85904170, 39.68080877, 820.8468), 0.0, 1),
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(44.83915179, 39.71144901, 813.3471), 69.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(44.69903495, 39.81048498, 812.2897), 0.0),

        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(45.15433004, 39.72076550, 967.2128), 0.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(45.08522124, 39.63377508, 885.2404), 240.0),
        StaticTarget(mpt.MPTarget.FACTORY, g.Position(45.10539653, 39.69182350, 938.0188), 352.0),
        StaticTarget(mpt.MPTarget.FUEL_FARM, g.Position(45.15632410, 39.72064390, 967.9493), 272.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(45.16017899, 39.85386545, 2322.5837), 104.0),

        StaticTarget(mpt.MPTarget.TRUCK, g.Position(44.84040893, 39.70881589, 812.6038), 20.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(44.84079339, 39.70799073, 813.5621), 20.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(44.84117768, 39.70716755, 814.3620), 20.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(44.84156342, 39.70634286, 813.7240), 20.0)

    ]
    scenario = ScenarioContainer('eastern_edge', 'Operation Eastern Edge',
                                 'OPRF event http://opredflag.com/events/941920?event_instance_id=16184118 ',
                                 OPFOR_defaults,
                                 (44.4, 39.6), (45.8, 40.9),
                                 'UBBN', 14.35, None, 60)
    scenario.add_static_targets(static_targets)
    return scenario


def _build_scenario_grafenwoehr() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(11.864007, 49.702781, 419.48), 357.0),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(11.870399, 49.699208, 417.0), 86.7),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(11.867393, 49.723289, 430.52), 0.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.830516, 49.647094, 429.59), 224.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.829567, 49.646533, 425.49), 224.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.828627, 49.645948, 423.62), 224.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.843982, 49.653030, 469.09), 328.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(11.889275, 49.650473, 443.89), 0.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(11.827306, 49.647393, 422.69), 42.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(11.828368, 49.647991, 429.24), 42.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(11.825846, 49.648443, 425.99), 42.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(11.826894, 49.649050, 433.09), 42.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(11.870667, 49.676053, 439.14), 76.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(11.860015, 49.676239, 442.84), 76.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(11.863381, 49.674388, 443.56), 76.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(11.873419, 49.672161, 435.93), 76.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.860676, 49.667383, 441.45), 64.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.867912, 49.664987, 433.60), 264.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.868944, 49.661477, 433.43), 156.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.883669, 49.693864, 431.57), 270.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.873286, 49.694864, 419.89), 270.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.859642, 49.696071, 421.83), 270.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.869534, 49.688501, 427.88), 255.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.883086, 49.687430, 428.82), 255.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(11.887024, 49.690743, 431.55), 260.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.867969, 49.708909, 440.98), 88.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.867912, 49.707930, 440.11), 88.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.859713, 49.709057, 439.09), 88.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.859615, 49.707660, 436.48), 88.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.871143, 49.716093, 428.80), 104.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.871457, 49.715246, 428.64), 104.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.871845, 49.714380, 428.47), 104.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.862382, 49.715188, 428.71), 104.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.862907, 49.714103, 429.15), 104.0),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(11.863426, 49.713109, 429.54), 104.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.792975, 49.673415, 471.79), 268.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.688999, 49.653852, 441.92), 0.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.681983, 49.648105, 445.55), 264.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(11.725696, 49.701803, 503.97), 0.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(11.660209, 49.658686, 472.56), 0.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(11.705820, 49.728982, 532.92), 0.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.722426, 49.682317, 469.97), 24.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.749247, 49.685396, 449.09), 48.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.696112, 49.704548, 549.71), 0.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.692646, 49.727015, 534.49), 0.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.728254, 49.723035, 496.91), 260.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.757636, 49.715390, 490.30), 0.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.815699, 49.682606, 456.92), 24.0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(11.795415, 49.706958, 446.28), 264.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(11.823934, 49.711619, 438.27), 0.0),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(11.939762, 49.700869, 413.32), 216.0),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(11.938873, 49.696791, 412.78), 308.0),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(11.753157, 49.637660, 408.07), 316.0),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(11.766913, 49.631381, 405.81), 352.0),
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(11.905241, 49.647916, 467.64), 284.1),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.681597, 49.713400, 517.89), 264.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.784881, 49.689039, 504.76), 264.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.787223, 49.728069, 452.00), 0.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.792347, 49.723495, 440.07), 272.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(11.754639, 49.701752, 456.66), 258.8),
    ]
    scenario = ScenarioContainer('grafenwoehr', 'Grafenwöhr shooting range',
                                 'https://de.wikipedia.org/wiki/Truppen%C3%BCbungsplatz_Grafenw%C3%B6hr',
                                 # https://www.bundeswehr.de/de/suche?typeahead=grafenw%C3%B6hr
                                 OPFOR_defaults,
                                 (11.5, 49.6), (12., 49.75),
                                 'ETIC', 3.8, None, 60)
    scenario.add_static_targets(static_targets)
    return scenario


def _build_scenario_vidsel() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(19.31797133, 66.39282943, 742.4591), 16.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(19.31513378, 66.39379173, 741.1738), 120.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31950926, 66.38849880, 692.8698), 144.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31904186, 66.38835333, 692.9743), 144.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31863179, 66.38822920, 690.5679), 144.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.30359813, 66.40229434, 622.5152), 144.0),
        StaticTarget(mpt.MPTarget.HANGAR, g.Position(19.31127886, 66.39751473, 670.2058), 48.0),
        StaticTarget(mpt.MPTarget.HANGAR, g.Position(19.31045241, 66.39787278, 667.6615), 48.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(19.30464126, 66.40767511, 657.9343), -24.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.31605033, 66.39240587, 740.9106), -168.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.30184618, 66.40491959, 627.9796), -96.0),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(19.54349283, 66.26228163, 476.9979), 72.0),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(19.27394310, 66.35653929, 459.9960), 24.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40638195, 66.30230167, 644.1675), -168.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40369599, 66.30258359, 651.8440), -168.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40321754, 66.29985320, 660.307), -168.0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(19.20640023, 66.31270131, 589.7704), -96.0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(19.20908678, 66.31575506, 594.6803), -24.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.20882646, 66.31381915, 592.3608), 0.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.21143275, 66.31377540, 592.9721), 0.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.21143915, 66.31202278, 590.7862), -52.0),
        StaticTarget(mpt.MPTarget.HARD_SHELTER, g.Position(19.21291479, 66.32204313, 606.6444), 0.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(19.21634312, 66.32102837, 609.8731), 0.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(19.21644677, 66.31984714, 609.7659), -88.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(19.21643993, 66.31920160, 605.4502), -88.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.21058105, 66.32030831, 603.9304), 0.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(19.20065322, 66.33814333, 638.4920), 96.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.19668719, 66.33828890, 639.4189), -24.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.19737021, 66.33849553, 638.9823), -24.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.19362982, 66.33854382, 633.8489), -24.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.19893407, 66.32989331, 616.1330), 120.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.48019603, 66.25903261, 600.9570), 0.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.46090564, 66.26839320, 658.0189), 0.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.46517561, 66.26676383, 652.0435), 0.0),
        StaticTarget(mpt.MPTarget.FUEL_FARM, g.Position(19.46220665, 66.26716873, 658.5374), 24.0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(19.47733422, 66.26138108, 618.4640), -72.0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(19.47423907, 66.26184409, 628.6353), -72.0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(19.45869373, 66.25975203, 631.5842), -72.0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(19.45983429, 66.26061953, 635.0258), -72.0),
        StaticTarget(mpt.MPTarget.FACTORY, g.Position(19.46658283, 66.25928029, 625.5227), 12.0),
        StaticTarget(mpt.MPTarget.POWER_PLANT, g.Position(19.45015968, 66.26757949, 659.2291), 48.0),
    ]
    scenario = ScenarioContainer('vidsel', 'Vidsel test range', 'Vidsel test range',
                                 OPFOR_defaults,
                                 (18.0, 65.7), (20.5, 67.0),
                                 'ESNJ', 8.8, None, 60)
    scenario.add_static_targets(static_targets)
    return scenario


# --------------------------- FOR TESTING --------------------------


def _build_scenario_test() -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(24.9792, 70.0577, 7), 0, 2)
    ]
    scenario = ScenarioContainer('Testing', 'Testing',  '',
                                 OPFOR_defaults,
                                 (20., 69.5), (27., 71.5),
                                 'ENNA', 12.3, None, 60)
    scenario.add_static_targets(static_targets)
    return scenario
