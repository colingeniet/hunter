"""Utility functions and constants shared between controller and worker."""
import datetime
from enum import IntEnum, unique
import logging
import logging.config
import multiprocessing as mp
import os
import os.path as osp
import queue
import threading
import time
from typing import Any, Optional


CTRL_CALLSIGN = 'xHunter'  # max 6 letters

MP_SERVER_HOST_DEFAULT = 'mpserver.opredflag.com'
PORT_TELNET_BASIS = 10000  # basic port for Telnet networking
PORT_HTTPD_BASIS = 9000  # basic port
PORT_MP_BASIS = 5000  # basic port for multiplayer networking

# Reserved positions in controller -> 5000 and 5001 can be used for local FG instance in parallel
PORT_MP_BASIS_CLIENT_CTRL = 5000  # basic port for multiplayer networking
INDEX_CONTROLLER = 2
INDEX_DYNAMIC_START = INDEX_CONTROLLER + 1
# Client ports in Worker are significantly higher to allow Controller's MP targets on same machine
PORT_MP_BASIS_CLIENT_WORKER = 5150

# Update frequencies
MP_TIMESTAMP_IGNORE_MAXDIFF = 10  # seconds (as specified in FGMS packets)

WORKER_POLLING_REGISTRATION_REQUEST = 10
WORKER_CTRL_POLLING = 10
GCP_MAX_WAIT_FOR_CTRL_REQUEST = 600  # seconds

MAX_DELTA_SHOOTABLE = 5  # seconds since last time MP data for this aircraft (attacker) was processed
POS_UPDATE_FREQ_MOVING_TARGET = 5  # every x seconds an update for moving mp_targets, trip  mp_targets, attackers
POS_UPDATE_FREQ_STATIC_TARGET = 5
SEND_POSITION_UPDATES_TO_DS = 5  # every x seconds send an update for the position and health to datastore

SESSION_MAX_IDLE_TIME_DEFAULT = 1800  # seconds

# mp properties hardcoded instead of efgms.FGMS_prop_code_by_name for performance and to get rid of dependency
# See http://wiki.flightgear.org/Multiplayer_protocol#Properties_Packet
MP_PROP_CHAT = 10002  # '/sim/multiplay/chat'
MP_PROP_LIVERY = 1101  # sim/model/livery/file
MP_PROP_GEAR_0_POSITION_NORM = 201
MP_PROP_GEAR_1_POSITION_NORM = 211
MP_PROP_GEAR_2_POSITION_NORM = 221
MP_PROP_ENGINE_0_N2 = 301
MP_PROP_ENGINE_0_RPM = 302
MP_PROP_ROTOR_MAIN = 800
MP_PROP_ROTOR_TAIL = 801
MP_PROP_GENERIC_BOOL_BASE = 11000  # sim/multiplay/generic/bool[0]
MP_PROP_GENERIC_STRING_BASE = 10100  # sim/multiplay/generic/string[0]
MP_PROP_GENERIC_FLOAT_BASE = 10200  # sim/multiplay/generic/float[0]
MP_PROP_GENERIC_INT_BASE = 10300  # sim/multiplay/generic/int[0]
MP_PROP_OPRF_SMOKE = MP_PROP_GENERIC_INT_BASE  # /sim/multiplay/generic/int[0]
MP_PROP_OPRF_RADAR = MP_PROP_GENERIC_INT_BASE + 2  # int[2] value 1 means silent, 0 means active (yes, so it is)
MP_PROP_OPRF_SELECT = MP_PROP_GENERIC_INT_BASE + 17 	# sim/multiplay/generic/int[17] - used in depot and hunter
MP_PROP_EMESARY_BRIDGE_BASE = 12000


def get_hunter_directory() -> str:
    """Determines the absolute path of the hunter root directory."""
    my_file = osp.realpath(__file__)
    my_dir = osp.split(my_file)[0]  # now we are in the hunter/hunter directory
    my_dir = osp.split(my_dir)[0]  # now we are in the root hunter directory
    return my_dir


SCENARIO_RESOURCES_DIR = 'scenario_resources'


def get_scenario_resources_dir() -> str:
    return osp.join(get_hunter_directory(), SCENARIO_RESOURCES_DIR)


def date_time_now() -> str:
    """Date and time as of now formatted as a string incl. seconds."""
    today = datetime.datetime.now()
    return today.strftime("%Y-%m-%d_%H%M%S")


class RuntimeFormatter(logging.Formatter):
    """A logging formatter which includes the delta time since start.

    Cf. https://stackoverflow.com/questions/25194864/python-logging-time-since-start-of-program
    """
    def __init__(self, *the_args, **kwargs) -> None:
        super().__init__(*the_args, **kwargs)
        self.start_time = time.time()

    def formatTime(self, record, datefmt=None):
        duration = datetime.datetime.utcfromtimestamp(record.created - self.start_time)
        elapsed = duration.strftime('%H:%M:%S')
        return "{}".format(elapsed)


def configure_logging(log_level: str, log_to_file: bool, main_process_name: str, gcp_logging: bool = False) -> None:
    """Set the logging level and maybe write to file.

    See also accepted answer to https://stackoverflow.com/questions/29015958/how-can-i-prevent-the-inheritance-
    of-python-loggers-and-handlers-during-multipro?noredirect=1&lq=1.
    And: https://docs.python.org/3.5/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
    """
    if gcp_logging:
        print('Asked for configuring GCP Logging')
        import hunter.gcp_utils as gu
        success = gu.configure_gcp_logging(log_level, main_process_name)
        if success:
            return
        # else we just fall back to native logging

    log_format = '%(processName)-10s %(threadName)s -- %(asctime)s - %(levelname)-9s: %(message)s'
    console_handler = logging.StreamHandler()
    fmt = RuntimeFormatter(log_format)
    console_handler.setFormatter(fmt)
    logging.getLogger().addHandler(console_handler)
    logging.getLogger().setLevel(log_level)
    if log_to_file:
        process_name = mp.current_process().name
        if process_name == 'MainProcess':
            file_name = '{}_main_{}.log'.format(main_process_name, date_time_now())
        else:
            file_name = '{}_process_{}_{}.log'.format(main_process_name, process_name, date_time_now())
        file_handler = logging.FileHandler(filename=file_name)
        file_handler.setFormatter(fmt)
        logging.getLogger().addHandler(file_handler)


def create_prop_for_string(key: str, value: str) -> str:
    return '--prop:string:/{}={}'.format(key, value)


def create_prop_for_int(key: str, value: int) -> str:
    return '--prop:int:/{}={}'.format(key, str(value))


def create_prop_for_double(key: str, value: float) -> str:
    return '--prop:double:/{}={}'.format(key, str(value))


def create_prop_for_float(key: str, value: float) -> str:
    return '--prop:float:/{}={}'.format(key, str(value))


def create_prop_for_bool(key: str, value: bool) -> str:
    return '--prop:bool:/{}={}'.format(key, str(value).lower())


def create_session_id() -> str:
    """The session id exchanged between controller and workers to makes sure they are in the same session.
    Nanoseconds since epoch should be pretty unique. Str due to PuBSub message attributes."""
    return str(time.time_ns())


@unique
class CloudIntegrationLevel(IntEnum):
    """The hierarchical level of cloud integration"""
    none = 0
    datastore = 1  # only store information in datastore
    pubsub = 2  # allow interaction with workers etc through messaging
    webui = 3  # controller is subordinate to web user interface


def parse_cloud_integration_level_as_int(int_level: int) -> CloudIntegrationLevel:
    for level in CloudIntegrationLevel:
        if int_level == level.value:
            return level
    raise ValueError('{} is not a recognized cloud integration level'.format(int_level))


def use_datastore(level: CloudIntegrationLevel) -> bool:
    return level is not CloudIntegrationLevel.none


def use_pubsub(level: CloudIntegrationLevel) -> bool:
    return level in (CloudIntegrationLevel.pubsub, CloudIntegrationLevel.webui)


def print_os_environ() -> None:
    for k, v in os.environ.items():
        print(f'{k}={v}')


class QueuedRunner(threading.Thread):
    """A thread basis class with incoming and outgoing queue, so the main process' run() method does not get halted.

    Adapted from chapter 'threaded Program Architecture' in chapter 14/page 420 of 'Python in a Nutshell'.
    """
    __slots__ = ('incoming_queue', 'outgoing_queue')

    def __init__(self, name: str) -> None:
        super().__init__(name=name, daemon=True)
        self.incoming_queue = queue.Queue()
        self.outgoing_queue = queue.Queue()

    def post_message(self, message) -> None:
        """Allow another thread (e.g. main thread having created this thread) to post a message."""
        logging.debug('Got a new message posted: %s', message)
        self.incoming_queue.put_nowait(message)

    def receive_message(self) -> Optional[Any]:
        """Allow another thread to get messages/results back.

        If there are no more messages in the queue, then None is returned.
        The caller must repeat calling this method until None is returned.
        """
        try:
            message = self.outgoing_queue.get_nowait()
            logging.debug('Client received a message from me: %s', message)
            return message
        except queue.Empty:
            pass  # nothing to do as expected to be empty once in a while
        return None

    def run(self) -> None:
        raise NotImplementedError
