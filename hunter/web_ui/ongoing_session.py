"""Handles all pages within an ongoing event."""
from typing import Any, List, Optional

from flask import Blueprint, render_template, request
from flask_menu import register_menu

import hunter.gcp_ds_io as gds
import hunter.gcp_ps_io as gps
import hunter.gcp_utils as gu
import hunter.messages as m
import hunter.scenarios as s

bp_ongoing_session = Blueprint('ongoing_session', __name__, url_prefix='/ongoing_session')

TITLE_CURRENT_SESSION = 'Current Session'
TITLE_DAMAGES = 'Damage and Hits'
TITLE_POSITIONS_LIST = 'List of Attacker and Target Positions'
TITLE_POSITIONS_MAP = 'Map of Attacker and Target Positions'
TITLE_CAPTAIN = 'Carrier Captain'

ds_client = gu.create_datastore_client()
publisher_client = gu.construct_publisher_client()


def airports_as_list_in_list(airports: List[s.AptDatAirport]) -> List[List[Any]]:
    """Transforms a list of airports to a list of lists directly usable in leaflet for markers"""
    if not airports:
        return list()

    leaflet_list = list()
    for apt in airports:
        if apt.kind == 16:
            kind = 'Sea base'
        elif apt.kind == 17:
            kind = 'Heliport'
        else:
            kind = 'Land airport'
        text = '<b>{}</b><br>{}<br>({})'.format(apt.icao, apt.name, kind)
        apt_data = [text, apt.lon, apt.lat]
        leaflet_list.append(apt_data)
    return leaflet_list


def positions_as_list_in_list(positions: Optional[List[m.PositionUpdate]],
                              hunter_session: gds.HunterSession) -> List[List[Any]]:
    """Cf. mp_targets.py amongst others from ca. line 135 for names/kinds"""
    if not positions:
        return list()
    leaflet_list = list()
    for position in positions:
        if position.kind == 'sack':
            continue
        my_name = position.kind
        if position.callsign.startswith(hunter_session.identifier) is False:
            colour = 'blue'  # will also be carriers and tankers
        elif position.kind in ['truck', 'buk-m2', 's-300']:
            colour = 'magenta'
        elif position.kind in ['missile-frigate', 'speedboat', 'lake_champlain', 'normandy', 'san_antonio',
                               'oliver_perry']:
            my_name = 'ship'  # override to hide which ships actually shoot
            colour = 'green'
        elif position.kind in ['containers', 'bridge', 'light_hangar', 'double_shelter', 'radar_station', 'bunker',
                               'gasometer', 'checkpoint', 'power_plant', 'oilrig',
                               'depot', 'warehouse', 'hard_shelter', 'hq', 'contarget',
                               'hill_target', 'cliff_target', 'water_target']:
            colour = 'red'
        else:
            colour = 'purple'
        text = '<b>{}</b><br>{}<br>{}<br>{}, {}'.format(position.callsign, my_name, position.health.name,
                                                        position.position.lon, position.position.lat)
        positions_data = [text, position.position.lon, position.position.lat, colour]
        leaflet_list.append(positions_data)
    return leaflet_list


@bp_ongoing_session.route('current_session')
@register_menu(bp_ongoing_session, 'ongoing_session.current_session', TITLE_CURRENT_SESSION, order=1)
def session():
    the_session = gds.query_not_terminated_session(ds_client)
    no_session = gds.valid_current_session(the_session) is False
    airports = list()
    if no_session:
        hunter_session = None
    else:
        hunter_session = gds.HunterSession.map_session_from_entity(the_session)
        airports = airports_as_list_in_list(hunter_session.scenario.airports)
    return render_template('ongoing_session/current_session.html', title=TITLE_CURRENT_SESSION, leaflet=True,
                           hunter_session=hunter_session, no_session=no_session, airports=airports,
                           damages_title=TITLE_DAMAGES, positions_list_title=TITLE_POSITIONS_LIST,
                           positions_map_title=TITLE_POSITIONS_MAP, captain_title=TITLE_CAPTAIN)


@bp_ongoing_session.route('damages')
@register_menu(bp_ongoing_session, '.ongoing_session.damages', TITLE_DAMAGES, order=2)
def damages():
    the_damages = gds.query_damage_results(ds_client, None)
    no_session = the_damages is None
    return render_template('ongoing_session/damages.html', title=TITLE_DAMAGES, damages=the_damages,
                           no_session=no_session)


@bp_ongoing_session.route('positions_list')
@register_menu(bp_ongoing_session, '.ongoing_session.positions_list', TITLE_POSITIONS_LIST, order=3)
def positions_list():
    the_positions = gds.query_current_session_positions(ds_client)
    no_session = the_positions is None
    return render_template('ongoing_session/positions_list.html', title=TITLE_POSITIONS_LIST, positions=the_positions,
                           no_session=no_session)


@bp_ongoing_session.route('positions_map')
@register_menu(bp_ongoing_session, '.ongoing_session.positions_map', TITLE_POSITIONS_LIST, order=4)
def positions_map():
    the_positions = gds.query_current_session_positions(ds_client)
    no_session = the_positions is None
    if no_session:
        hunter_session = None
        positions = list()
    else:
        the_session = gds.query_not_terminated_session(ds_client)
        hunter_session = gds.HunterSession.map_session_from_entity(the_session)
        positions = positions_as_list_in_list(the_positions, hunter_session)
    return render_template('ongoing_session/positions_map.html', title=TITLE_POSITIONS_MAP, leaflet=True,
                           positions=positions,
                           no_session=no_session, hunter_session=hunter_session)


@bp_ongoing_session.route('captain', methods=['GET', 'POST'])
@register_menu(bp_ongoing_session, 'ongoing_session.captain', TITLE_CAPTAIN, order=5)
def captain():
    no_session = False
    if request.method == 'GET':
        return render_template('ongoing_session/captain.html', title=TITLE_CAPTAIN, no_session=no_session)

    # else we are handling the POST
    course_type = int(request.form['course_type'])
    navigate_course_deg = int(request.form['navigate_course_deg'])
    deck_lights = False if request.form.get('deck_lights') is None else True
    flood_lights = False if request.form.get('flood_lights') is None else True

    if gps.send_action_captain_order(publisher_client, course_type, navigate_course_deg,
                                     deck_lights, flood_lights):

        order_result = 'The captain\'s order has been successfully placed.'
    else:
        order_result = 'There was an error processing the captain\'s order.'

    return render_template('ongoing_session/captain.html', title=TITLE_CAPTAIN, no_session=no_session,
                           order_result=order_result)
