var HEADING_MARGIN = 5; # constant for allowable deviation in heading
var ROLL_MARGIN = 3; # constant for allowable deviation in roll
var roll_target = 90;
var heading_target = 180;
var right_turn = 1;
var level_out = 0;
var pos_aileron = 0;
var pos_elevator = 0;

var turn_plane = func {
  var current_heading = getprop("/orientation/heading-magnetic-deg");
  var min = heading_target - HEADING_MARGIN;
  if (min < 0) {
    min = min + 360;
  }
  var max = heading_target + HEADING_MARGIN;
  if (max > 360) {
    max = max - 360;
  }
  var do_stop = 0;
  # print("Current heading = ", current_heading, ", heading target = ", heading_target, ", elevator position = ", pos_elevator, ", min = ", min, ", max = ", max);
  if (max > min) {
    if (current_heading > min and current_heading < max) {
      do_stop = 1;
    }
  }
  else {
    if (current_heading < max or current_heading > min) {
      do_stop = 1;
    }
  }
  if (do_stop) {
    setprop("/controls/flight/elevator", 0);
    turn_timer.stop();
    # level the plane
    print("Leveling");
    level_out = 1;
    setprop("/controls/flight/aileron", -1 * pos_aileron);
    bank_timer.start();
  }
}

turn_timer = maketimer(0.1, turn_plane);

var bank_plane = func {
  var current_roll = abs(getprop("/orientation/roll-deg"));
  # print("Current roll = ", current_roll, ", roll target = ", roll_target, ", level out = ", level_out);
  if (level_out) {
    if (current_roll < ROLL_MARGIN) {
      # print("Final roll for levelling out ", current_roll);
      bank_timer.stop();
      setprop("/controls/flight/aileron", 0);
      # enable autopilot modes incl. new target_heading
      setprop("/autopilot/locks/altitude", "altitude-hold");
      setprop("/autopilot/settings/heading-bug-deg", heading_target);
      setprop("/autopilot/locks/heading", "dg-heading-hold");
    }
  } else {
    if (current_roll >= (roll_target - ROLL_MARGIN)) {
      # print("Final roll for bank ", current_roll);
      bank_timer.stop();
      setprop("/controls/flight/aileron", 0);
      print("Turning");
      setprop("/controls/flight/elevator", pos_elevator);
      turn_timer.start();
    }
  }
}

bank_timer = maketimer(0.05, bank_plane);

var performance_turn = func(target_heading, target_roll, aileron_pos, elevator_pos) {
  pos_elevator = -1 * abs(elevator_pos);  # negative = nose up
  heading_target = abs(target_heading);
  gui.popupTip("Making performance turn");
  var current_heading = getprop("/orientation/heading-deg");
  if (abs(current_heading - target_heading) < 2 * HEADING_MARGIN) {
    # print("Difference between current heading ", current_heading, " and target heading ", target_heading, " is too small.");
    setprop("/autopilot/settings/heading-bug-deg", heading_target);
    return; # no need for a tight turn
  }
  if (heading_target > current_heading) {
    if (abs(heading_target - current_heading) < 180) {
      right_turn = 1;
    } else {
      right_turn = 0;
    }
  }
  else {
    if (abs(heading_target - current_heading) < 180) {
      right_turn = 0;
    } else {
      right_turn = 1;
    }
  }
  if (right_turn) {
    print("Turning right");
  }
  else {
    print("Turning left");
  }
  roll_target = abs(target_roll); # just to be sure
  if (right_turn) {
    pos_aileron = abs(aileron_pos);
  } else {
    pos_aileron = -1 * abs(aileron_pos); # negative = against the clock / to the left
  }
  # disable autopilot modes
  setprop("/autopilot/locks/altitude", "");
  setprop("/autopilot/locks/heading", "");

  # roll the plane
  print("Banking");
  level_out = 0;
  setprop("/controls/flight/aileron", pos_aileron);
  bank_timer.start();
}

var fail_systems = func (probability) {
  # based on damage.nas -> fail_systems
  var failure_modes = FailureMgr._failmgr.failure_modes;
  var mode_list = keys(failure_modes);
  var failed = 0;
  foreach(var failure_mode_id; mode_list) {
      if (rand() < probability) {
          FailureMgr.set_failure_level(failure_mode_id, 1);
          failed += 1;
      }
  }
  return failed;
}
