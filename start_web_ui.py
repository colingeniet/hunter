import hunter.gcp_utils as gu

if __name__ == '__main__':
    gu.check_minimal_gcp_env_variables()
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.

    from hunter.web_ui import app  # Need to do import late, otherwise env variable check is too late due to imports
    app.run(host='127.0.0.1', port=8080, debug=True)
